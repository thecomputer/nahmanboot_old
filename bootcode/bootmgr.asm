ORG 0x800
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%define original_pic1_base 0x8
%define original_pic2_base 0x70

next_sector:

cli
jmp check_a20line_main


error_a20line_msg db "Can't enable A20 line",0

;;; check A20 line is enabled

; Function: check_a20
; Purpose: to check the status of the a20 line in a completely self-contained state-preserving way.
;          The function can be modified as necessary by removing push's at the beginning and their
;          respective pop's at the end if complete self-containment is not required.
; Returns: 0 in ax if the a20 line is disabled (memory wraps around)
;          1 in ax if the a20 line is enabled (memory does not wrap around)
check_a20:
	pushf
	cli ; disabling interrupts
	push ds
	push es
	push di
	push si

	xor ax, ax
	mov es, ax
 
	not ax ; ax = 0xFFFF
	mov ds, ax
 
	mov di, 0x0500
	mov si, 0x0510
 
	mov al, byte [es:di]
	push ax
 
	mov al, byte [ds:si]
	push ax
 
	mov byte [es:di], 0x00
	mov byte [ds:si], 0xFF
 
	cmp byte [es:di], 0xFF
 
	pop ax

	mov byte [ds:si], al
 
	pop ax
	mov byte [es:di], al
 
	mov ax, 0
	je check_a20_exit
 
	mov ax, 1
 
check_a20_exit:
	pop si
	pop di
	pop es
	pop ds
	popf
	ret


; Function: enable_a20line
; Purpose: try to enable A20 Line first with BIOS interrupt and if it's not working - with the Keyboard controller. If failed - stop execution.
; Returns: nothing

enable_a20line:


	;; Try to enable A20 line with bios
	mov     ax,2403h                ;--- A20-Gate Support ---
	int     15h
	jb      a20_failed                  ;INT 15h is not supported
	cmp     ah,0
	jnz     a20_failed                  ;INT 15h is not supported
	 
	mov     ax,2402h                ;--- A20-Gate Status ---
	int     15h
	jb      a20_failed              ;couldn't get status
	cmp     ah,0
	jnz     a20_failed              ;couldn't get status
	 
	cmp     al,1
	jz      a20_activated           ;A20 is already activated
	 
	mov     ax,2401h                ;--- A20-Gate Activate ---
	int     15h
	jb      a20_failed             ;couldn't activate the gate
	cmp     ah,0
	jnz     a20_failed              ;couldn't activate the gate

	a20_failed:
	call enable_a20line_keyboard ; try to enable A20 line with keyboard controller
	call check_a20
	cmp ax,1 ; A20 line is activated
	jmp a20_activated
	
	a20_fault:
		;call clear_screen
		;lea bx,[error_a20line_msg]
		;call print_string
		jmp $ ; stop execution because of failure

	a20_activated:	
	ret

; Function: enable_a20line
; Purpose: try to enable A20 Line with Keyboard controller.
; Returns: nothing

enable_a20line_keyboard:
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        sti
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret


check_a20line_main:

	
	
	call check_a20
	
	cmp ax,1 ; 0 = a20 line is disabled, 1 = enabled
	je stage4_framebuffer ; proceed to next stage
	call enable_a20line ; otherwise enable a20line


	
	jmp 0:$


;;; setup framebuffer

stage4_framebuffer:

	;; get frame buffer address
cli



push es
    push ax
    push di
    push cx
    push ebx

    mov ax,01000h
    mov es,ax

    xor di,di
    mov ax,4F01h
    mov cx,118h
    ;mov cx,11Bh    
    int 10h

    mov di,40 ; we will take the address out of the info
    ;mov ebx, dword [es:di]

    ;mov dword [0xA00:0x000], ebx

    pop ebx
    pop cx
    pop di    
    pop ax    
pop es 

;mov bx,0xdead
;	jmp 0:$

	mov bx, 0x103
	;mov bx,0x11B
	or bx,0x4000

	mov ax,0x4f02
	int 10h

;;; setup protected mode

; clear Segments before we move to protected mode
	mov ax,0x0
	mov ds,ax
	mov es,ax
	mov fs,ax
	mov gs,ax


	cli
	
	;; Setting up GDTR for Protected mode
lgdt [gdt_pointer] ; load the gdt table


mov eax, cr0 
or eax,0x1 ; set the protected mode bit on special CPU reg cr0
mov cr0, eax


jmp 0x8:boot2 ; long jump to the code segment

gdt_start:
    dq 0x0
gdt_code:
    dw 0xFFFF
    dw 0x0
    db 0x0
    db 10011010b
    db 11001111b
    db 0x0
gdt_data:
    dw 0xFFFF
    dw 0x0
    db 0x0
    db 10010010b
    db 11001111b
    db 0x0
gdt_end:

gdt_pointer:
    dw gdt_end - gdt_start
    dd gdt_start
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

;;; print menu based MBR table

bits 32

boot2:

	cli

	mov esp,0x4000 ; important! set a sane value here

	mov ax, 0x10
    mov ds, ax
    mov es, ax
    ;mov fs, ax
    ;mov gs, ax
    mov ss, ax
	
	;mov bx,0xcafe
	call build_screen

	mov cx,0
;; init IDT & ISRs

jmp after_idtr

idt:

TIMES 256*8 db 0

idtr:

dw 256*8-1
dd idt

after_idtr:


    lea eax,[double_fault_isr]
    mov [idt+8*8],ax
    mov word [idt+8*8+2],0x8
    mov byte [idt+8*8+4],0
    mov byte [idt+8*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+8*8+6],ax
    
    lea eax,[keyboard_isr]
    mov [idt+0x21*8],ax
    mov word [idt+0x21*8+2],0x8
    mov byte [idt+0x21*8+4],0
    mov byte [idt+0x21*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x21*8+6],ax

    lea eax,[pit_isr]
    mov [idt+0x20*8],ax
    mov word [idt+0x20*8+2],0x8
    mov byte [idt+0x20*8+4],0
    mov byte [idt+0x20*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x20*8+6],ax

    lea eax,[video_isr]
    mov [idt+0x49*8],ax
    mov word [idt+0x49*8+2],0x8
    mov byte [idt+0x49*8+4],0
    mov byte [idt+0x49*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x49*8+6],ax

    lea eax,[disk_isr]
    mov [idt+0x59*8],ax
    mov word [idt+0x59*8+2],0x8
    mov byte [idt+0x59*8+4],0
    mov byte [idt+0x59*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x59*8+6],ax
    ;int 79

lidt [idtr]

;; mask imr

mov dx,0x21
mov al,0xFC
out dx,al

in al,0x20
mov ah,al
in al,0xa0

%define init 0x11
%define init 0x11

%define offset1 0x21
%define offset2 0x29

%define slavepic 4
%define slavepic2 2

%define pic8086mode 0x1

push ax

mov al,init
out 0x20,al
out 0xA0,al

mov al,0x20
out 0x21,al

mov al,0x28
out 0xA1,al

mov al,slavepic
out 0x21,al

mov al,slavepic2
out 0xa1,al

mov al,0x1
out 0x21,al
out 0xA1,al

pop ax

;mov dx,0xa0
;out dx,al
;mov al,ah
;mov dx,0x20
;out dx,al


;sti ; time to restore interrupts...

;; init video ISR vars

mov ebx,[0x10000+40]
mov [screen_buffer_addr],ebx
lea ebx,[arr]
mov [screen_font_addr],ebx

mov ax,0xA
int 0x49 ; prepare screen bottom bar

lea ecx,[string]
mov ax,0x0
int 0x49

mov ax,0xB
int 0x49

lea ecx,[string1]
mov ax,0x0
int 0x49



;lea ecx,[string]
;mov ax,0x0

;int 0x49







;lea ecx,[string3]
;mov ax,0x0
;;
;int 0x49



;lea ecx,[string4]
;mov ax,0x0

;int 0x49

;lea ecx,[string5]
;mov ax,0x0

;int 0x49

;lea ecx,[string6]
;mov ax,0x0

;int 0x49

;lea ecx,[string7]
;mov ax,0x0

;int 0x49

;lea ecx,[string8]
;mov ax,0x0

;int 0x49

;lea ecx,[string9]
;mov ax,0x0

;int 0x49



;;;;;;; init PS/2 Controller & Keyboard


;mov edi,[0x10000+40]
;lea ebx,[arr]
;add ebx,32+66
;call print_char


;;;;;;; init PS/2 Controller & Keyboard


mov dx,0x64
mov al,0xAD
out dx,al
mov al,0xA7
out dx,al

mov dx,0x60
in al,dx



mov dx,0x64
mov al,0x20
out dx,al

;in al,0x60
;jmp 0x8:$
;and al,1110001b

;push ax

;mov dx,0x64
;mov al,0x60
;out dx,al

;pop ax

mov dx,0x64
mov al,0x60
out dx,al

mov al,1110001b
mov dx,0x60
out dx,al

mov dx,0x64
mov al,0xAA
out dx,al

mov dx,0x60

.loop_wait_for_selftest:
	in al,dx
	cmp al,0x55
	jne .loop_wait_for_selftest

mov dx,0x64
mov al,0xAB
out dx,al

mov dx,0x60
.loop_wait_for_test1:
	in al,dx
	cmp al,0x0
	jne .loop_wait_for_test1


mov dx,0x64
mov al,0xA9
out dx,al

mov dx,0x60
.loop_wait_for_test2:
	in al,dx
	cmp al,0x0
	jne .loop_wait_for_test2

mov dx,0x64
mov al,0xAE
out dx,al

mov dx,0x64
mov al,0xA8
out dx,al

mov dx,0x64
mov al,0xF5
out dx,al


;; main program, displaying menu and such

lea ebx,[keyboard_buffer]

sti

menu:

mov al,byte [keyboard_buffer]
mov ah,byte [keyboard_buffer+1]

	cmp ah,0x1
	je reboot
	
	cmp ah,0xBC
	je F2_boot_default

menu_back:

mov byte [keyboard_buffer+1],0
xor ax,ax

jmp menu

	F2_boot_default: ; pressed F2
		cli
		lea ecx,[string10]
		mov ax,0
		int 0x49
		mov edx,0xFFFC
		jmp select_partition		

reboot:  ; try to do 8042-reset, otherwise triple-fault (no ACPI support)

cli
lea ecx,[string9]
mov ax,0
int 0x49

call acpi_reboot

call delay_cpu

.wait_gate1:
    in al,64h
    and al,2
    jnz .wait_gate1
    mov al,0D1h
    out 64h,al
.wait_gate2:
    in al,64h
    and al,2
    jnz .wait_gate2
    mov al,0FEh
    out 60h,al              ; keyboard RESET

lidt [zero] 
int 70h

hlt

zero:
dq 0

;;; select partition
;;; non function, edx = partition number
;;; exceptions (edx=0xFFFC - default bootable partition)

select_partition:

mov ebx,0x600+0x1be ; entry to first partition

cmp edx,0xFFFC
je default_partition_loading

hlt ; problem

default_partition_loading:

;;CheckPartitions           ; Check Partition Table For Bootable Partition
    ;lea ebx,[PToff]              ; Base = Partition Table Entry 1
    ;mov ebx,0x600+0x1be
    ;mov cx, 4                 ; There are 4 Partition Table Entries
.CKPTloop:
      ;mov al, BYTE [ebx]       ; Get Boot indicator bit flag
      ;test al, 0x80           ; Check For Active Bit
      ;jnz .CKPTFound          ; We Found an Active Partition
      ;add bx, 0x10            ; Partition Table Entry is 16 Bytes
      ;dec cx                  ; Decrement Counter
      
      ;loop .CKPTloop           ; Loop

	;mov ax,0xDEAD
    ;jmp ERROR                 ; ERROR!

.CKPTFound:
      ;mov cx, bx    ; Save Offset
      ;add bx, 8               ; Increment Base to LBA Address

;; ReadVBR
    ;mov ebx, [ebx]       ; Start LBA of Active Partition - save it for loading later
	xor ebx,ebx
	mov bx,[default_lba]

;;; dropping to realmode

lgdt [gdt2_pointer]
jmp 0x8:return_realmode





ERROR:

hlt

default_lba dw 0x1000 ;;; default LBA partition of NahmanOS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;; ISR & Exceptions Handlers ;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%define yellow 0x2b
%define purple_blue 0x21
%define pink 0x23
%define purple 0x22
%define white 0x1f
%define green 0x7A
%define dark_purple 0x6A
%define dark_purple_blue 0x69
%define dark_blue 0x68
%define navy_blue 0x7f
%define bsod_blue 0x1
%define darker_blue 0xb0
%define gray 0x7
%define black 0x0
%define red 0x4
%define red2 028

string_double_fault db "NahmanBoot panic: fatal error, double faulting. Halting",0
string db  "Extended Nahman Bootloader initialized. Copyright (C) 2019 Liav Albani.",0
string1 db  "Detected GPT table! Please choose a partition to boot... STUB for now!",0
string2 db "Booting selected partition!",0
string3 db "Boot parition 1",0
string4 db "Boot partition 2",0
string5 db "Boot partition 3",0
string6 db "Boot partition 4",0
string7 db "View (logical) extended partitions",0
string8 db "Help & About",0
string9 db "Initiating reboot in a second...",0
string10 db "Booting default bootable partition...",0

string_bar db "ESC: Reboot, F1: Help, F2: Boot default, ENTER\Space: Boot Selected Partition",0

double_fault_isr:

	;cli
	;call clear_screen

	lea ecx,[string_double_fault]
	mov ax,0x1
	int 0x49

	mov ecx,0xdeadbeef
	mov edx,0xcafebabe
	cli
	hlt

fs_isr:

	iretd
disk_isr:

	iretd


keyboard_buffer db 0
times 100 db 0

keyboard_isr:

	push eax
	push ebx
	push ecx

	mov ebx,0xdeadc0de
	in al,0x60
	mov ah,al
	in al,0x60

	inc byte [keyboard_buffer]
	lea ebx,[keyboard_buffer]	
	inc ebx

	;mov ecx,ebx	
	;.loop_put_info:
	
	;add ebx,2

	;loop .loop_put_info

	mov word [ebx],ax

	;lea ecx,[string9]
	;mov ax,0x0
	;int 0x49

push ax
push dx
mov al,0x20
mov dx,0x20
out dx,al
pop dx
pop ax
;sti

pop ecx
pop ebx
pop eax
iretd



video_isr:

	;jmp 0x8:$
	jmp start_video_isr

	row_number db 0
	column_number db 0
	screen_buffer_addr dd 0
	screen_font_addr dd 0
	color_font db white

	start_video_isr:

	cmp ax,0 ; print string
	je .isr_pring_string

	cmp ax,1 ; print string fault
	je .isr_pring_string_fault

	cmp ax,2 ; clear_line
	je .isr_clear_line

	;cmp ax,3 ; transfer data from other framebuffer

	;cmp ax,4 ; clear_screen

	cmp ax,0xA
	je .isr_build_bar

	cmp ax,0xB
	je .isr_inc_row_number

	cmp ax,5 ; change font address , ebx = address of new font array
	je .isr_change_font
	cmp ax,6 ; change framebuffer address , ebx = address of framebuffer
	je .isr_change_framebuffer_addr
	;jmp end_video_isr

	jmp end_video_isr

.isr_pring_string:
	
	;call clear_line
	mov byte [column_number],0
	call print_string_isr2

	jmp end_video_isr

.isr_pring_string_fault:
	call build_screen
	mov byte [column_number],0
	mov byte [color_font],black
	mov byte [row_number],39
	call print_string_isr2
	jmp end_video_isr

.isr_clear_line:
	call clear_line
	jmp end_video_isr	

.isr_change_font:
	mov [screen_font_addr],ebx
	jmp end_video_isr

.isr_change_framebuffer_addr:
	mov [screen_buffer_addr],ebx
	jmp end_video_isr

.isr_build_bar:

	
	lea ecx,[string_bar]
	push ax
	mov ah, byte [color_font]	
	mov al, byte [row_number]
	
	mov byte [color_font],black
	mov byte [row_number],39
	
	call print_string_isr2
	mov byte [row_number],al
	mov byte [color_font],ah	
	pop ax
	jmp end_video_isr

.isr_inc_row_number:
	inc byte [row_number]
	jmp end_video_isr

end_video_isr:

	push ax
	push dx
	mov al,0x20
	out 0x20,al
	pop dx
	pop ax
	iretd

;;; ecx - addr of string
;;; returns void
print_string_isr2:
push ebp
push ecx
push edi
push ebx 
push eax

mov edi,[screen_buffer_addr]
mov ebx,[screen_font_addr]

add ebx,32

push ecx

	mov ecx,[row_number]
	cmp ecx,0
	je exit_loop_choose_row

	loop_choose_row:

		add edi,800*15

	loop loop_choose_row

	exit_loop_choose_row:
	
	xor ecx,ecx

	;mov byte [column_number],
	mov cl, [column_number]
	;mov cl,2
	;cmp cl,0
	;je exit_loop_choose_col

	;loop_choose_col:

	;	add edi,8*3

	;loop loop_choose_col

	;exit_loop_choose_col:

	;jmp 0x8:$

;mov byte [column_number],0

pop ecx

mov ebp,edi

loop_print_string2:
	push ebx
	add edi,2
	
	cmp word [ecx], "\n"
	jne cont_printing2
	mov edi,ebp
	add edi,800*15
	mov ebp,edi
	add ecx,2
	inc byte [row_number]
	mov byte [column_number],0
	jmp reloop_print2
	cont_printing2:
	push edx
	push ecx

	xor edx,edx
	mov eax,16
	mov cl,byte [ecx]
	mul cl		

	pop ecx	
	pop edx

	add ebx,eax
	call print_char
	inc byte [column_number]
	
	cmp byte [column_number],80
	jne cont_no_inc_row

	mov edi,ebp
	add edi,800*15
	mov ebp,edi
	inc byte [row_number]
	mov byte [column_number],0
	cont_no_inc_row:

	add edi, 8
	add ecx,1
	
	reloop_print2:
	pop ebx
	cmp byte [ecx],0
	jne loop_print_string2

inc byte [row_number]

cmp byte [row_number],37
jng cont_no_zeroing_row

mov byte [row_number],0

cont_no_zeroing_row:

pop eax
pop ebx
pop edi
pop ecx
pop ebp
ret

;;; ecx - number_line
;;; returns void
clear_line:
push ecx
push edi
push ebx 
push eax
push edx

mov edi,[screen_buffer_addr]

push ecx

	mov ecx,[row_number]
	cmp ecx,0
	je exit_loop_choose_row2

	loop_choose_row2:

		add edi,768*4*15

	loop loop_choose_row2
	;sub edi,768*4*15
	exit_loop_choose_row2:

	

pop ecx



	mov dh,6
	mov dl,9
	mov al,11
	
	push ecx
    	push ebx

	
	mov ecx,768*4*10

    	.loop_write_pixels2:
        	
		mov byte [edi],al
        	inc edi
		mov byte [edi],dl
		inc edi 
		mov byte [edi],dh
		inc edi

		;add edi,768

        	loop .loop_write_pixels2
    

    	pop ebx 
    	pop ecx
	
pop edx
pop eax
pop ebx
pop edi
pop ecx
ret


;;;;;;;;;;;;;;;

pit_isr:

push ax
push dx
mov al,0x20
out 0x20,al
pop dx
pop ax

iret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;; General Functions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;
print_char:

push ebp
mov ebp,esp
push eax
push ebx
push edx
push ecx
push edi

	mov ecx,16

loop_print_char_all:

push ecx
push eax
push edi
	mov ecx,8
	rol al,4
	mov al,byte [ebx]
	add edi,7
loop_print_char:
	push edi
	mov ah,0x1
	and ah,al
	cmp ah,0
	je continue_printing_char
	
	push edx
	push eax
	
	mov al,byte [color_font]
	mov byte [edi],al
	
	pop eax
	pop edx

	continue_printing_char:
	pop edi
	shr al,1
	sub edi,1
	
	loop loop_print_char

pop edi
pop eax
pop ecx

	add edi,800
	add ebx,1

	loop loop_print_char_all


pop edi
pop ecx
pop edx
pop ebx
pop eax
pop ebp
	ret


arr db 114,181,74,134,00,00,00,00,32,00,00,00,01,00,00,00,00,01,00,00,16,00,00,00,16,00,00,00,08,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,126,129,165,129,129,189,153,129,129,126,00,00,00,00,00,00,126,255,219,255,255,195,231,255,255,126,00,00,00,00,00,00,00,00,108,254,254,254,254,124,56,16,00,00,00,00,00,00,00,00,16,56,124,254,124,56,16,00,00,00,00,00,00,00,00,24,60,60,231,231,231,24,24,60,00,00,00,00,00,00,00,24,60,126,255,255,126,24,24,60,00,00,00,00,00,00,00,00,00,00,24,60,60,24,00,00,00,00,00,00,255,255,255,255,255,255,231,195,195,231,255,255,255,255,255,255,00,00,00,00,00,60,102,66,66,102,60,00,00,00,00,00,255,255,255,255,255,195,153,189,189,153,195,255,255,255,255,255,00,00,30,14,26,50,120,204,204,204,204,120,00,00,00,00,00,00,60,102,102,102,102,60,24,126,24,24,00,00,00,00,00,00,63,51,63,48,48,48,48,112,240,224,00,00,00,00,00,00,127,99,127,99,99,99,99,103,231,230,192,00,00,00,00,00,00,24,24,219,60,231,60,219,24,24,00,00,00,00,00,128,192,224,240,248,254,248,240,224,192,128,00,00,00,00,00,02,06,14,30,62,254,62,30,14,06,02,00,00,00,00,00,00,24,60,126,24,24,24,126,60,24,00,00,00,00,00,00,00,102,102,102,102,102,102,102,00,102,102,00,00,00,00,00,00,127,219,219,219,123,27,27,27,27,27,00,00,00,00,00,124,198,96,56,108,198,198,108,56,12,198,124,00,00,00,00,00,00,00,00,00,00,00,254,254,254,254,00,00,00,00,00,00,24,60,126,24,24,24,126,60,24,126,00,00,00,00,00,00,24,60,126,24,24,24,24,24,24,24,00,00,00,00,00,00,24,24,24,24,24,24,24,126,60,24,00,00,00,00,00,00,00,00,00,24,12,254,12,24,00,00,00,00,00,00,00,00,00,00,00,48,96,254,96,48,00,00,00,00,00,00,00,00,00,00,00,00,192,192,192,254,00,00,00,00,00,00,00,00,00,00,00,36,102,255,102,36,00,00,00,00,00,00,00,00,00,00,16,56,56,124,124,254,254,00,00,00,00,00,00,00,00,00,254,254,124,124,56,56,16,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,24,60,60,60,24,24,24,00,24,24,00,00,00,00,00,102,102,102,36,00,00,00,00,00,00,00,00,00,00,00,00,00,00,108,108,254,108,108,108,254,108,108,00,00,00,00,24,24,124,198,194,192,124,06,06,134,198,124,24,24,00,00,00,00,00,00,194,198,12,24,48,96,198,134,00,00,00,00,00,00,56,108,108,56,118,220,204,204,204,118,00,00,00,00,00,48,48,48,96,00,00,00,00,00,00,00,00,00,00,00,00,00,12,24,48,48,48,48,48,48,24,12,00,00,00,00,00,00,48,24,12,12,12,12,12,12,24,48,00,00,00,00,00,00,00,00,00,102,60,255,60,102,00,00,00,00,00,00,00,00,00,00,00,24,24,126,24,24,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,24,24,24,48,00,00,00,00,00,00,00,00,00,00,126,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,24,24,00,00,00,00,00,00,00,00,02,06,12,24,48,96,192,128,00,00,00,00,00,00,124,198,198,206,222,246,230,198,198,124,00,00,00,00,00,00,24,56,120,24,24,24,24,24,24,126,00,00,00,00,00,00,124,198,06,12,24,48,96,192,198,254,00,00,00,00,00,00,124,198,06,06,60,06,06,06,198,124,00,00,00,00,00,00,12,28,60,108,204,254,12,12,12,30,00,00,00,00,00,00,254,192,192,192,252,06,06,06,198,124,00,00,00,00,00,00,56,96,192,192,252,198,198,198,198,124,00,00,00,00,00,00,254,198,06,06,12,24,48,48,48,48,00,00,00,00,00,00,124,198,198,198,124,198,198,198,198,124,00,00,00,00,00,00,124,198,198,198,126,06,06,06,12,120,00,00,00,00,00,00,00,00,24,24,00,00,00,24,24,00,00,00,00,00,00,00,00,00,24,24,00,00,00,24,24,48,00,00,00,00,00,00,00,06,12,24,48,96,48,24,12,06,00,00,00,00,00,00,00,00,00,126,00,00,126,00,00,00,00,00,00,00,00,00,00,96,48,24,12,06,12,24,48,96,00,00,00,00,00,00,124,198,198,12,24,24,24,00,24,24,00,00,00,00,00,00,124,198,198,198,222,222,222,220,192,124,00,00,00,00,00,00,16,56,108,198,198,254,198,198,198,198,00,00,00,00,00,00,252,102,102,102,124,102,102,102,102,252,00,00,00,00,00,00,60,102,194,192,192,192,192,194,102,60,00,00,00,00,00,00,248,108,102,102,102,102,102,102,108,248,00,00,00,00,00,00,254,102,98,104,120,104,96,98,102,254,00,00,00,00,00,00,254,102,98,104,120,104,96,96,96,240,00,00,00,00,00,00,60,102,194,192,192,222,198,198,102,58,00,00,00,00,00,00,198,198,198,198,254,198,198,198,198,198,00,00,00,00,00,00,60,24,24,24,24,24,24,24,24,60,00,00,00,00,00,00,30,12,12,12,12,12,204,204,204,120,00,00,00,00,00,00,230,102,102,108,120,120,108,102,102,230,00,00,00,00,00,00,240,96,96,96,96,96,96,98,102,254,00,00,00,00,00,00,195,231,255,255,219,195,195,195,195,195,00,00,00,00,00,00,198,230,246,254,222,206,198,198,198,198,00,00,00,00,00,00,124,198,198,198,198,198,198,198,198,124,00,00,00,00,00,00,252,102,102,102,124,96,96,96,96,240,00,00,00,00,00,00,124,198,198,198,198,198,198,214,222,124,12,14,00,00,00,00,252,102,102,102,124,108,102,102,102,230,00,00,00,00,00,00,124,198,198,96,56,12,06,198,198,124,00,00,00,00,00,00,255,219,153,24,24,24,24,24,24,60,00,00,00,00,00,00,198,198,198,198,198,198,198,198,198,124,00,00,00,00,00,00,195,195,195,195,195,195,195,102,60,24,00,00,00,00,00,00,195,195,195,195,195,219,219,255,102,102,00,00,00,00,00,00,195,195,102,60,24,24,60,102,195,195,00,00,00,00,00,00,195,195,195,102,60,24,24,24,24,60,00,00,00,00,00,00,255,195,134,12,24,48,96,193,195,255,00,00,00,00,00,00,60,48,48,48,48,48,48,48,48,60,00,00,00,00,00,00,00,128,192,224,112,56,28,14,06,02,00,00,00,00,00,00,60,12,12,12,12,12,12,12,12,60,00,00,00,00,16,56,108,198,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,255,00,00,48,48,24,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,120,12,124,204,204,204,118,00,00,00,00,00,00,224,96,96,120,108,102,102,102,102,124,00,00,00,00,00,00,00,00,00,124,198,192,192,192,198,124,00,00,00,00,00,00,28,12,12,60,108,204,204,204,204,118,00,00,00,00,00,00,00,00,00,124,198,254,192,192,198,124,00,00,00,00,00,00,56,108,100,96,240,96,96,96,96,240,00,00,00,00,00,00,00,00,00,118,204,204,204,204,204,124,12,204,120,00,00,00,224,96,96,108,118,102,102,102,102,230,00,00,00,00,00,00,24,24,00,56,24,24,24,24,24,60,00,00,00,00,00,00,06,06,00,14,06,06,06,06,06,06,102,102,60,00,00,00,224,96,96,102,108,120,120,108,102,230,00,00,00,00,00,00,56,24,24,24,24,24,24,24,24,60,00,00,00,00,00,00,00,00,00,230,255,219,219,219,219,219,00,00,00,00,00,00,00,00,00,220,102,102,102,102,102,102,00,00,00,00,00,00,00,00,00,124,198,198,198,198,198,124,00,00,00,00,00,00,00,00,00,220,102,102,102,102,102,124,96,96,240,00,00,00,00,00,00,118,204,204,204,204,204,124,12,12,30,00,00,00,00,00,00,220,118,102,96,96,96,240,00,00,00,00,00,00,00,00,00,124,198,96,56,12,198,124,00,00,00,00,00,00,16,48,48,252,48,48,48,48,54,28,00,00,00,00,00,00,00,00,00,204,204,204,204,204,204,118,00,00,00,00,00,00,00,00,00,195,195,195,195,102,60,24,00,00,00,00,00,00,00,00,00,195,195,195,219,219,255,102,00,00,00,00,00,00,00,00,00,195,102,60,24,60,102,195,00,00,00,00,00,00,00,00,00,198,198,198,198,198,198,126,06,12,248,00,00,00,00,00,00,254,204,24,48,96,198,254,00,00,00,00,00,00,14,24,24,24,112,24,24,24,24,14,00,00,00,00,00,00,24,24,24,24,00,24,24,24,24,24,00,00,00,00,00,00,112,24,24,24,14,24,24,24,24,112,00,00,00,00,00,00,118,220,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,16,56,108,198,198,198,254,00,00,00,00,00,00,00,60,102,194,192,192,192,194,102,60,12,06,124,00,00,00,00,204,00,00,204,204,204,204,204,204,118,00,00,00,00,00,12,24,48,00,124,198,254,192,192,198,124,00,00,00,00,00,16,56,108,00,120,12,124,204,204,204,118,00,00,00,00,00,00,204,00,00,120,12,124,204,204,204,118,00,00,00,00,00,96,48,24,00,120,12,124,204,204,204,118,00,00,00,00,00,56,108,56,00,120,12,124,204,204,204,118,00,00,00,00,00,00,00,00,60,102,96,96,102,60,12,06,60,00,00,00,00,16,56,108,00,124,198,254,192,192,198,124,00,00,00,00,00,00,198,00,00,124,198,254,192,192,198,124,00,00,00,00,00,96,48,24,00,124,198,254,192,192,198,124,00,00,00,00,00,00,102,00,00,56,24,24,24,24,24,60,00,00,00,00,00,24,60,102,00,56,24,24,24,24,24,60,00,00,00,00,00,96,48,24,00,56,24,24,24,24,24,60,00,00,00,00,00,198,00,16,56,108,198,198,254,198,198,198,00,00,00,00,56,108,56,00,56,108,198,198,254,198,198,198,00,00,00,00,24,48,96,00,254,102,96,124,96,96,102,254,00,00,00,00,00,00,00,00,00,110,59,27,126,216,220,119,00,00,00,00,00,00,62,108,204,204,254,204,204,204,204,206,00,00,00,00,00,16,56,108,00,124,198,198,198,198,198,124,00,00,00,00,00,00,198,00,00,124,198,198,198,198,198,124,00,00,00,00,00,96,48,24,00,124,198,198,198,198,198,124,00,00,00,00,00,48,120,204,00,204,204,204,204,204,204,118,00,00,00,00,00,96,48,24,00,204,204,204,204,204,204,118,00,00,00,00,00,00,198,00,00,198,198,198,198,198,198,126,06,12,120,00,00,198,00,124,198,198,198,198,198,198,198,124,00,00,00,00,00,198,00,198,198,198,198,198,198,198,198,124,00,00,00,00,00,24,24,126,195,192,192,192,195,126,24,24,00,00,00,00,00,56,108,100,96,240,96,96,96,96,230,252,00,00,00,00,00,00,195,102,60,24,255,24,255,24,24,24,00,00,00,00,00,252,102,102,124,98,102,111,102,102,102,243,00,00,00,00,00,14,27,24,24,24,126,24,24,24,24,24,216,112,00,00,00,24,48,96,00,120,12,124,204,204,204,118,00,00,00,00,00,12,24,48,00,56,24,24,24,24,24,60,00,00,00,00,00,24,48,96,00,124,198,198,198,198,198,124,00,00,00,00,00,24,48,96,00,204,204,204,204,204,204,118,00,00,00,00,00,00,118,220,00,220,102,102,102,102,102,102,00,00,00,00,118,220,00,198,230,246,254,222,206,198,198,198,00,00,00,00,00,60,108,108,62,00,126,00,00,00,00,00,00,00,00,00,00,56,108,108,56,00,124,00,00,00,00,00,00,00,00,00,00,00,48,48,00,48,48,96,192,198,198,124,00,00,00,00,00,00,00,00,00,00,254,192,192,192,192,00,00,00,00,00,00,00,00,00,00,00,254,06,06,06,06,00,00,00,00,00,00,192,192,194,198,204,24,48,96,206,155,06,12,31,00,00,00,192,192,194,198,204,24,48,102,206,150,62,06,06,00,00,00,00,24,24,00,24,24,24,60,60,60,24,00,00,00,00,00,00,00,00,00,54,108,216,108,54,00,00,00,00,00,00,00,00,00,00,00,216,108,54,108,216,00,00,00,00,00,00,17,68,17,68,17,68,17,68,17,68,17,68,17,68,17,68,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,221,119,221,119,221,119,221,119,221,119,221,119,221,119,221,119,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,248,24,24,24,24,24,24,24,24,24,24,24,24,24,248,24,248,24,24,24,24,24,24,24,24,54,54,54,54,54,54,54,246,54,54,54,54,54,54,54,54,00,00,00,00,00,00,00,254,54,54,54,54,54,54,54,54,00,00,00,00,00,248,24,248,24,24,24,24,24,24,24,24,54,54,54,54,54,246,06,246,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,00,00,00,00,00,254,06,246,54,54,54,54,54,54,54,54,54,54,54,54,54,246,06,254,00,00,00,00,00,00,00,00,54,54,54,54,54,54,54,254,00,00,00,00,00,00,00,00,24,24,24,24,24,248,24,248,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,248,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,31,00,00,00,00,00,00,00,00,24,24,24,24,24,24,24,255,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,255,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,31,24,24,24,24,24,24,24,24,00,00,00,00,00,00,00,255,00,00,00,00,00,00,00,00,24,24,24,24,24,24,24,255,24,24,24,24,24,24,24,24,24,24,24,24,24,31,24,31,24,24,24,24,24,24,24,24,54,54,54,54,54,54,54,55,54,54,54,54,54,54,54,54,54,54,54,54,54,55,48,63,00,00,00,00,00,00,00,00,00,00,00,00,00,63,48,55,54,54,54,54,54,54,54,54,54,54,54,54,54,247,00,255,00,00,00,00,00,00,00,00,00,00,00,00,00,255,00,247,54,54,54,54,54,54,54,54,54,54,54,54,54,55,48,55,54,54,54,54,54,54,54,54,00,00,00,00,00,255,00,255,00,00,00,00,00,00,00,00,54,54,54,54,54,247,00,247,54,54,54,54,54,54,54,54,24,24,24,24,24,255,00,255,00,00,00,00,00,00,00,00,54,54,54,54,54,54,54,255,00,00,00,00,00,00,00,00,00,00,00,00,00,255,00,255,24,24,24,24,24,24,24,24,00,00,00,00,00,00,00,255,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,63,00,00,00,00,00,00,00,00,24,24,24,24,24,31,24,31,00,00,00,00,00,00,00,00,00,00,00,00,00,31,24,31,24,24,24,24,24,24,24,24,00,00,00,00,00,00,00,63,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,255,54,54,54,54,54,54,54,54,24,24,24,24,24,255,24,255,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,248,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,31,24,24,24,24,24,24,24,24,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,00,00,00,00,00,00,00,255,255,255,255,255,255,255,255,255,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,255,255,255,255,255,255,255,00,00,00,00,00,00,00,00,00,00,00,00,00,00,118,220,216,216,216,220,118,00,00,00,00,00,00,120,204,204,204,216,204,198,198,198,204,00,00,00,00,00,00,254,198,198,192,192,192,192,192,192,192,00,00,00,00,00,00,00,00,254,108,108,108,108,108,108,108,00,00,00,00,00,00,00,254,198,96,48,24,48,96,198,254,00,00,00,00,00,00,00,00,00,126,216,216,216,216,216,112,00,00,00,00,00,00,00,00,102,102,102,102,102,124,96,96,192,00,00,00,00,00,00,00,118,220,24,24,24,24,24,24,00,00,00,00,00,00,00,126,24,60,102,102,102,60,24,126,00,00,00,00,00,00,00,56,108,198,198,254,198,198,108,56,00,00,00,00,00,00,56,108,198,198,198,108,108,108,108,238,00,00,00,00,00,00,30,48,24,12,62,102,102,102,102,60,00,00,00,00,00,00,00,00,00,126,219,219,219,126,00,00,00,00,00,00,00,00,00,03,06,126,219,219,243,126,96,192,00,00,00,00,00,00,28,48,96,96,124,96,96,96,48,28,00,00,00,00,00,00,00,124,198,198,198,198,198,198,198,198,00,00,00,00,00,00,00,00,254,00,00,254,00,00,254,00,00,00,00,00,00,00,00,00,24,24,126,24,24,00,00,255,00,00,00,00,00,00,00,48,24,12,06,12,24,48,00,126,00,00,00,00,00,00,00,12,24,48,96,48,24,12,00,126,00,00,00,00,00,00,14,27,27,27,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,216,216,216,112,00,00,00,00,00,00,00,00,24,24,00,126,00,24,24,00,00,00,00,00,00,00,00,00,00,118,220,00,118,220,00,00,00,00,00,00,00,56,108,108,56,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,24,24,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,24,00,00,00,00,00,00,00,00,15,12,12,12,12,12,236,108,108,60,28,00,00,00,00,00,216,108,108,108,108,108,00,00,00,00,00,00,00,00,00,00,112,216,48,96,200,248,00,00,00,00,00,00,00,00,00,00,00,00,00,124,124,124,124,124,124,124,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,226,128,136,255,226,152,186,255,226,152,187,255,226,153,165,255,226,153,166,255,226,153,163,255,226,153,160,255,226,128,162,255,226,151,152,255,226,151,139,255,226,151,153,255,226,153,130,255,226,153,128,255,226,153,170,255,226,153,171,226,153,172,255,226,152,188,255,226,150,182,226,150,186,255,226,151,128,226,151,132,255,226,134,149,255,226,128,188,255,194,182,255,194,167,255,226,150,172,255,226,134,168,255,226,134,145,255,226,134,147,255,226,134,146,255,226,134,144,255,226,136,159,226,140,153,255,226,134,148,255,226,150,178,255,226,150,188,255,32,194,160,226,128,128,226,128,129,226,128,130,226,128,131,226,128,132,226,128,133,226,128,134,226,128,135,226,128,136,226,128,137,226,128,138,226,128,175,255,33,255,34,255,35,255,36,255,37,255,38,255,39,255,40,255,41,255,42,255,43,255,44,255,45,255,46,255,47,255,48,255,49,255,50,255,51,255,52,255,53,255,54,255,55,255,56,255,57,255,58,255,59,255,60,255,61,255,62,255,63,255,64,255,65,255,66,255,67,255,68,255,69,255,70,255,71,255,72,255,73,255,74,255,75,255,76,255,77,255,78,255,79,255,80,255,81,255,82,255,83,255,84,255,85,255,86,255,87,255,88,255,89,255,90,255,91,255,92,255,93,255,94,255,95,255,96,255,97,255,98,255,99,255,100,255,101,255,102,255,103,255,104,255,105,255,106,255,107,255,108,255,109,255,110,255,111,255,112,255,113,255,114,255,115,255,116,255,117,255,118,255,119,255,120,255,121,255,122,255,123,255,124,255,125,255,126,255,226,140,130,255,195,135,255,195,188,255,195,169,255,195,162,255,195,164,255,195,160,255,195,165,255,195,167,255,195,170,255,195,171,255,195,168,255,195,175,255,195,174,255,195,172,255,195,132,255,195,133,226,132,171,255,195,137,255,195,166,255,195,134,255,195,180,255,195,182,255,195,178,255,195,187,255,195,185,255,195,191,255,195,150,255,195,156,255,194,162,255,194,163,255,194,165,255,226,130,167,255,198,146,255,195,161,255,195,173,255,195,179,255,195,186,255,195,177,255,195,145,255,194,170,255,194,186,255,194,191,255,226,140,144,255,194,172,255,194,189,255,194,188,255,194,161,255,194,171,255,194,187,255,226,150,145,255,226,150,146,255,226,150,147,255,226,148,130,255,226,148,164,255,226,149,161,255,226,149,162,255,226,149,150,255,226,149,149,255,226,149,163,255,226,149,145,255,226,149,151,255,226,149,157,255,226,149,156,255,226,149,155,255,226,148,144,255,226,148,148,255,226,148,180,255,226,148,172,255,226,148,156,255,226,148,128,255,226,148,188,255,226,149,158,255,226,149,159,255,226,149,154,255,226,149,148,255,226,149,169,255,226,149,166,255,226,149,160,255,226,149,144,255,226,149,172,255,226,149,167,255,226,149,168,255,226,149,164,255,226,149,165,255,226,149,153,255,226,149,152,255,226,149,146,255,226,149,147,255,226,149,171,255,226,149,170,255,226,148,152,255,226,148,140,255,226,150,136,255,226,150,132,255,226,150,140,255,226,150,144,255,226,150,128,255,206,177,255,195,159,206,178,255,206,147,255,207,128,255,206,163,255,207,131,255,194,181,206,188,255,207,132,255,206,166,255,206,152,255,206,169,226,132,166,255,206,180,255,226,136,158,255,207,134,226,136,133,226,140,128,255,206,181,226,136,136,255,226,136,169,255,226,137,161,255,194,177,255,226,137,165,255,226,137,164,255,226,140,160,255,226,140,161,255,195,183,255,226,137,136,255,194,176,255,226,136,153,226,139,133,255,194,183,255,226,136,154,255,226,129,191,255,194,178,255,226,136,142,226,150,160,255,194,160,255


build_screen:

	call clear_screen_flat
	call build_screen_bar
	ret

build_screen_bar:
	push ebx
	push esi
	push eax
	push edx
	push ecx
	
	mov ebx,0xfc000000;[0x10000+40]
	add ebx,(600-18)*800
	;xor esi,esi
	;mov si,word [0x10000+2+2+4*2+4]
	
    	mov ecx,800*18
    	.loop_write_pixels1:
        	
		mov byte [ebx],gray;0xB0
        	inc ebx
		
        	loop .loop_write_pixels1

pop ecx
pop edx
pop eax
pop esi
pop ebx

ret
	

clear_screen_flat:
	push ebx
	push esi
	push eax
	push edx
	push ecx
	
	mov ebx,[0x10000+40]
	xor esi,esi
	mov si,word [0x10000+2+2+4*2+4]
	
    	mov ecx,800*600
    	.loop_write_pixels1:
        	
		mov byte [ebx],bsod_blue;0xB0
        	inc ebx
		
        	loop .loop_write_pixels1
    


	

pop ecx
pop edx
pop eax
pop esi
pop ebx

ret


;;;;;;;;;;;;;;;;


delay_cpu:
push ecx

mov ecx,30000
	loop_wait1:
		push ecx
		mov ecx,300
			loop_wait2:
			nop
			loop loop_wait2
		nop
		pop ecx

	loop loop_wait1

pop ecx
ret

;;;;;;;;;;;;;;; need to implement, partly stub for now

acpi_reboot:
	; detecting rsdp

		;; trying to search "RSD PTR " from 0xE0000 to 0xFFFFF
	
		mov ebx,0xE0000
		mov ecx, 8191
		lea edi,[checksum]		

		.loop_searching_checksum:
		
			push ecx
			push ebx
			push edi

			mov ecx,8
			.loop_compare_string:
				mov dl,byte [edi]
				cmp [ebx],dl
				jne .exit_loop_compare_string
				inc edi
				inc ebx
			loop .loop_compare_string
			jmp .exit_loop_finding_ebx		

			.exit_loop_compare_string:

			pop edi
			pop ebx
			pop ecx
			add ebx,0x10	
		loop .loop_searching_checksum

		;; trying to search "RSD PTR " in EBDA

	.exit_loop_finding_ebx:

			pop edi
			pop ebx
			pop ecx
	;;; check revision
	mov dl, byte [ebx+15] ; revision
	cmp dl,2
	jl .exit_acpi_reboot ; less than 2 is bad... abort if so

	;;; validating table (important!)

	mov dl, byte [ebx+8] ; checksum!

	jmp .exit_acpi_reboot ; didn't work, abort

	; find acpi version
	cmp ax,2
	jl .exit_acpi_reboot ; less than 2 is bad... abort if so



	.exit_acpi_reboot:
	ret

checksum db "RSD PTR ",0

BITS 16

newidt:
dq 0
dq 0

return_realmode:
;;; drop back to real mode (remapping the PIC, reset CPU to realmode)
nop
cli

mov     ax,0x0010               ; 8.9.2. Step 4. 
        mov     ds,ax 
        mov     es,ax 
        mov     fs,ax 
        mov     gs,ax 
        mov     ss,ax 
	
	mov     ax,0x03FF               ; 8.9.2. Step 5. 
        mov     word [newidt],ax 
        xor     ax,ax 
        mov     word [newidt+2],ax 
        mov     word [newidt+4],ax 
        

	mov     eax,cr0                 ; 8.9.2. Step 6. 
        and     eax,0x7FFFFFFE
        mov     cr0,eax 



	jmp 0:complete_realmode ; 8.9.2. Step 7. 

complete_realmode:
nop
	lidt    [newidt]
	xor     ax,ax                   ; 8.9.2. Step 8. 
        mov     ds,ax 
        mov     es,ax 
        mov     fs,ax 
        mov     gs,ax 
        mov     ss,ax

	

;;; return to text mode

mov ax,0x3
int 0x10

	;hlt

;;; load VBR of partition at 0x7c00

	call ReadSectors2 ;; we assume ebx is loaded with the correct LBA

	;hlt
;;; jump to VBR

jmp 0x0:0x7c00 ; good luck from here!

jmp $

;; Defining Disk Address Packet Structure
	daps2 db 10h
	db 0 ; always 0
	daps2_sectors dw 1 ; number of sectors to load - 7
	daps2_offset dw 0x0 ; offset to transfer
	daps2_segment dw 0x7c0 ; segment to transfer
	daps2_lba dq 1 ; lower 32 bits of 48Bit LBA

	ReadSectors2:
		push esi
	    lea esi, [daps2]
	    mov [daps2_lba],ebx
	    call load_data_from_disk2
		pop esi
	    ret

	; Function: load_data_from_disk
	; Parameters: no parameters
	; Purpose: Load Data from Disk
	; Returns: nothing
	load_data_from_disk2:
	    push ax
	    push dx
	    mov ah,0x42
	    mov dl,0x80
	    int 13h
	    pop dx
	    pop ax
		ret

gdt2_start:
    dq 0x0
gdt2_code:
    dw 0xFFFF
    dw 0x0
    db 0x0
    db 10011010b
    db 00001111b
    db 0x0
gdt2_data:
    dw 0xFFFF
    dw 0x0
    db 0x0
    db 10010010b
    db 00001111b
    db 0x0
gdt2_end:

gdt2_pointer:
    dw gdt2_end - gdt2_start
    dd gdt2_start
CODE_SEG equ gdt2_code - gdt2_start
DATA_SEG equ gdt2_data - gdt2_start
