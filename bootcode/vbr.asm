;Copyright 2019 Liav Albani

;Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

;The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

main:
BITS 16
jmp short start
nop


; This Bootloader is intended to test various things before loading kernel:
; 1. Simple graphics mode enabled? Do it (80x25)
; 2. Test architecture. System is allowed to boot only on i586 and above!
; 3. A20 line enabled?
; 4. Preparing IMS (Info Memory Segment) for kernel usage
; 5. Mapping Memory allocated and store in IMS
; 6. Mapping usable graphical modes (Experimental?), store in IMS
; 7. Enumarate PCI bus, checking for other usable hardware, store in IMS
; 8. Sending all the information to an allocated memory segment called IMS 
; 9. Load the kernel executable after every test with paramters about IMS address...
; 10. Pass control to kernel

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;; Init Boot Environmnet ;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; First of all, we have to set many procedures to initialize some sort of pre-kernel environment..
;; We have to set a offset with Extra Segment register...

start:



	mov cx,4

	;xor ebx,ebx
	;hlt

	;CKPTloop:
      	;mov al, BYTE [si]       ; Get Boot indicator bit flag
      	;test al, 0x80           ; Check For Active Bit
      	;jnz CKPTFound          ; We Found an Active Partition
      	;add bx, 0x10            ; Partition Table Entry is 16 Bytes
      	;dec cx                  ; Decrement Counter
      	;loop CKPTloop           ; Loop

	;CKPTFound:
	;mov cx, si    ; Save Offset
      	;add si, 8               ; Increment Base to LBA Address

	
	;mov ebx, dword [si]

	
	

	jmp start_booting

	loading_str db "Booting!",0

start_booting:


	mov ax, 07C0h		; Set up 4K stack space after this bootloader
	add ax, 288		; (4096 + 512) / 16 bytes per paragraph
	mov ss, ax
	mov sp, 4096

	mov ax, 07C0h		; Set data segment to where we're loaded
	mov ds, ax

	mov dword [offset_partition_lba],ebx

	;; $si is pointing to partition table
	;; We need to find the offset of our partition 
	mov ebp,ebx ; reserve ESI in EBP
	

;;; to do - fix the code so it will search for 800x600 16 color mode by checking that with the appropriate VBE call
	;mov ax,0x4f02
	;mov ebx,0103h
	;int 10h


    mov ax,0x1000    
    mov es,ax         ; Set ES with 0x1000
	
; Let's load the Superblock (EXT2 filesystem)
	mov dword [daps1_lba],2 ; 512 bytes per block * 2 blocks
        lea si, [daps1]
	call load_data_from_disk

; Check block size of ext2 blocks from Superblock
	mov ecx,dword [es:0x18]
	mov ebx,1024
 	shl bx,cl
	mov dword [size_of_block_ext2],ebx

	mov ax,word [es:0x38]
	cmp ax,0xef53 ; check if magic number of ext2 is present
	je continue
	jmp $ ; failure - not suitable filesystem (What?!)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;; Search for load2.bin File ;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; Let's load stage 2....
    ;; For that mission, we need to get inode table address, fetch inode 2 (root directory)
    ;; Check the inode 2 blocks for load2.bin and the corresponding inode of it...Easy!!
    ;; Then get back to the inode table, see where is the data, load it up... Get Set.. Fire the second stage of the bootloader!


;; Defining Disk Address Packet Structure - for Inode table!

offset_partition_lba dd 0

daps1 db 10h
db 0 ; always 0
daps1_sectors dw 4 ; number of sectors to load - 4
daps1_offset dw 0 ; offset to transfer
daps1_segment dw 0x1000 ; segment to transfer
daps1_lba dq 2 ; lower 32 bits of 48Bit LBA

size_of_block_ext2 dd 0

; Function: load_data_from_disk
; Parameters: no parameters
; Purpose: Load Data from Disk
; Returns: nothing
load_data_from_disk:
    push ax
    push dx
    push ebx

    mov ebx,[offset_partition_lba]
    add [daps1_lba],ebx

    mov ah,0x42
    mov dl,0x80
    int 13h
    jc $ ; not working...
    
    pop ebx
    pop dx
    pop ax
ret

; Function: load_blocks_inode
; Parameters: Assuming DAPS is ready, GS:BX contains the segment of inode and offset starting with pointers
; Purpose: Load Data from Disk, according to inode block pointers
; Returns: EDI = amount of blocks loaded
load_blocks_inode:

mov cx,11 ; check all direct block pointers
    xor edi,edi ; EDI will be a counter of how many blocks we loaded from inode 2 block pointers
    jmp loop_check_block_pointers

    load_blocks:

	push ebx

	mov ebx,edx
	mov eax, dword [size_of_block_ext2]
	mul ebx ; EAX * EBX = Block LBA * size_of_block_ext2
	mov ebx, 512
	div ebx ; EAX / EBX = Block group*size_of_block_ext2/512 bytes of LBA
	mov [daps1_lba],eax ;; EAX is ready, was filled with LBA of direct pointer
	
	pop ebx
	mov eax, dword [size_of_block_ext2]
	push eax
         lea si, [daps1]
         call load_data_from_disk
	pop eax
	 add word [daps1_offset],ax
         inc di
         jmp continue_check_block_pointers
    loop_check_block_pointers:
         mov edx, dword [gs:bx] ; get block of a direct block pointer in inode 2
         cmp edx, 0
         jne load_blocks
         continue_check_block_pointers:
         add bx,4
         loop loop_check_block_pointers

ret

; Function: load_ext2block_sectors
; Parameters: no parameters
; Purpose: Load DAPS Sectors value with the appropriate value
; Returns: no returns
load_ext2block_sectors:
	push edx
	push eax
    	push cx
		xor dx,dx
    		mov eax,[size_of_block_ext2]
		mov cx,512
		div cx
		mov word [daps1_sectors],ax ; load only 1 logical ext2 block of data
    	pop cx
    	pop eax
	pop edx
ret

;;;;;;;;;;;;;;;;;;;;;;;END OF FUNCTIONS;;;;;;;;;;;;;;;;;;;;;;

continue:
;;;;;;;;;;;;;;;; Loading inode table from group block data ;;;;;;;;;;;;;;;;;
    ;; let's prepare some addersing registers for later use. EBX will take the inode table address
    ;; EDI will be a free-willy addressing register (will be used for inodes fetchings) 
    
    mov ebx,dword [es:0x408] ; get the inode table LBA
	mov eax, dword [size_of_block_ext2]

	mul ebx ; EAX * BX = Block group * size_of_block_ext2
	mov ebx, 512
	div ebx ; EAX / BX = Block group*size_of_block_ext2/512 bytes of LBA
    mov [daps1_lba],eax ;; EAX is ready, was filled with LBA of first inode table

    mov dx,0x1500
    mov gs,dx ; gs will contain the segment where we will load the inode table

    mov word [daps1_segment],gs
    lea si, [daps1]
    call load_data_from_disk

;;;;;;;;;;;;;;;; Loading inode 2 block pointers ;;;;;;;;;;;;;;;;;

    ;; we need now to reach inode 2 - which is the root directory...
    mov si,word [es:88] ; get size of inode structure from Superblock
    xor bx,bx ; zero bx
    add bx,si ; we want to access inode 2, so we add the size of one inode sturcture
    add bx, 40 ; Then we want to access the first direct block pointer in inode 2

    mov word [daps1_segment],0x2000 ; prepare segment to load..
    mov word [daps1_offset],0
    
	call load_ext2block_sectors
    
    lea si, [daps1]
    
    call load_blocks_inode
         

exit_loop_check_block_pointers:

;;;;;;;;;;;;;;;; Search load2.bin File ;;;;;;;;;;;;;;;;;

;; search the root directory for "load2.bin" file and its inode number

    mov eax,dword [size_of_block_ext2] ; get size of block as written in superblock
    mul di ; multiply by amount of blocks we loaded
    mov cx, ax ; move result to cx (this will be our counter for searching)

    mov bx,0x2000
    mov fs,bx
    xor bx,bx
    
    lea si,[file_to_load]

loop_check_string:

	push bx
	push si
	loop_check_equal:
		mov dl,[fs:bx]
		cmp byte [si],0
		je exit_success_check_equal
		cmp dl, byte [si]	
		jne continue_loop_check_equal
		inc bx
		inc si
		jmp loop_check_equal
        continue_loop_check_equal:        
	pop si	
	pop bx
	
	inc bx

        loop loop_check_string
	
	mov bx,0xFFFF ; failure!
	jmp 0x7c0:$ ; didn't found any file...	

exit_success_check_equal:
pop si
pop bx ; BX contains the offset of filename string


	mov word [daps1_segment],0x07C0
	mov word [daps1_offset],512
	mov dword [daps1_lba],1 ; 512 bytes per block * 1 blocks
        lea si, [daps1]
	call load_data_from_disk
	
	mov dx,0x3000
	mov gs,dx ; gs will contain the segment where we will load the inode table
	xor dx,dx

	jmp 0x07C0:0x0200 ; continue bootloader of next sector.

file_to_load db "load.bin",0x0

	times 510-($-$$) db 0	; Pad remainder of boot sector with 0s
	dw 0xAA55		; The standard PC boot signature

continue_2:

;;;;;;;;;;;;;;;; Search load2.bin Inode ;;;;;;;;;;;;;;;;;

;;;; Loading Corresponding Block Group Descriptor ;;;;;
	sub bx,8 ; subtract 8 bytes, so we can access the inode number

	mov edi,dword [fs:bx] ; move inode number to EDX
	mov esi,dword [es:0x28] ; move number of inodes per block group to ESI
	sub edi,1 ; (inode-1)
	mov eax,edi
	div esi ; (EAX) block group =divide (EDI=EAX=inode-1) with ESI (INODES_PER_GROUP)

	mov ebx, dword [size_of_block_ext2]
	mul ebx ; EAX * EBX = Block group * size_of_block_ext2
	mov ebx, 512
	div bx ; EAX / BX = Block group*size_of_block_ext2/512 bytes of LBA
	add eax,4 ; add to base LBA the block group LBA offset,  base LBA is 4 (offset 2048 bytes / 512 bytes of LBA)


	mov word [ds:daps1_segment],0x3000
	mov word [ds:daps1_offset],0
	
	call load_ext2block_sectors
	
	mov dword [ds:daps1_lba],eax ; LBA of block group offset
        lea si, [ds:daps1]
	call load_data_from_disk
	xor edx,edx
	
;;;; Loading Inode Table With Correct Block ;;;;;

	mov ebx,dword [gs:0x8] ; get the base inode table LBA from the loaded block group descriptor
	
	mov eax,edi ; mov (Inode-1) to EAX
        mov esi,dword [es:0x28] ; move number of inodes per block group to ESI
	div esi ; edx will contain index
	
	xor eax,eax

	mov edi, edx ; move Index (EDX) to EDI
	xor edx,edx
	mov ax, word [es:88] ; Inode size from superblock
	mul edi ; EAX = (EDI) index * (AX) Inode size
	mov edi, dword [ds:size_of_block_ext2]
	div edi ; EAX (final result) = EAX ((index * Inode size)) / EDI ((size_of_block_ext2))
	add ebx,eax ; Add to base inode table LBA the amount of blocks to "skip" to reach our inode
	;jmp $
;;;;;;;;;;;;;;;; Load load2.bin Inode Structure ;;;;;;;;;;;;;;;;;

	mov ecx,edx ; edx contains how many bytes we need to skip in the block, move it to ECX
	xor edx,edx ; zero EDX

	mov eax, edi ; move (EDI) size_of_block_ext2 to EAX
	
	mul ebx ; EAX * EBX = Block LBA * size_of_block_ext2
	mov ebx, 512
	div ebx ; EAX / EBX = Block group*size_of_block_ext2/512 bytes of LBA
	mov [daps1_lba],eax ;; EAX is ready, was filled with LBA of direct pointer

	mov word [daps1_segment],0x5000
	mov dword [daps1_lba],eax ; LBA of block group offset
        lea si, [daps1]
	call load_data_from_disk

	mov dx,0x5000
	mov gs,dx ; gs will contain the segment where we loaded our inode structure
	xor dx,dx
	mov bx,cx ; we want to access the first direct block pointer in the inode. cx contains how many bytes we need to skip in the block, move it to bx
	mov edx,dword [gs:bx]

;;;;;;;;;;;;;;; Load load2.bin Data blocks ;;;;;;;;;;;;;;;;;;;;;

    	mov word [daps1_segment],0xA00 ; prepare segment to load..
    	mov word [daps1_offset],0x0
    
	call load_ext2block_sectors

    	lea si, [daps1]
    	mov cx,11 ; check all direct block pointers
	
	add bx,0x28 ; get to the first indirect pointer...

    	xor edi,edi ; EDI will be a counter of how many blocks we loaded from inode 2 block pointers
	call load_blocks_inode

mov esi,ebp

	
	
    jmp 0x0:0xA000 ; Jump to 0x00:0xA000
    jmp $

load_load2bin_here:
	OFFSET equ load_load2bin_here - main
