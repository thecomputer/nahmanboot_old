#dd if=/dev/zero of=image.img bs=100M count=1

#sudo /sbin/mkfs.ext2 ./image.img

#nasm -f bin -o load.bin load.asm
#nasm -f bin -o load2.bin load2.asm

nasm -f bin -o binaries/vbr.bin bootcode/vbr.asm

nasm -f bin -o binaries/load.bin bootcode/load.asm
nasm -f bin -o binaries/init.bin initcode/init.asm
#touch init.bin

nasm -f bin bootcode/mbr.asm -o binaries/mbr.bin
nasm -f bin bootcode/bootmgr.asm -o binaries/bootmgr.bin

dd status=noxfer conv=notrunc if=binaries/mbr.bin of=../hdd.img bs=1 seek=0
dd status=noxfer conv=notrunc if=binaries/bootmgr.bin of=../hdd.img bs=1 seek=1048576
dd status=noxfer conv=notrunc if=binaries/vbr.bin of=../hdd.img bs=1 seek=2097152

#sudo losetup -o 1048576 /dev/loop0 ../hdd.img

sudo cp binaries/load.bin /mnt/load.bin
sudo cp binaries/init.bin /mnt/init.bin

#qemu-system-i386 -drive file=../hdd.img,index=0,media=disk,format=raw -m 512MB -cpu qemu32 -monitor telnet:127.0.0.1:55555,server,nowait;

#qemu-system-i386 -drive file=../hdd.img,index=0,media=disk,format=raw -m 512MB -cpu qemu32 -no-shutdown -no-reboot -monitor stdio; #
#qemu-system-i386 -device isa-debug-exit -machine type=i440fx,accel=kvm -vga cirrus -drive file=../hdd.img,if=ide,index=0,media=disk,format=raw,cache=none -m 512m -cpu qemu32 -no-shutdown -no-reboot -monitor stdio; #
#qemu-system-i386 -device isa-debug-exit -machine pc -vga cirrus -drive file=../hdd.img,if=ide,index=0,media=disk,format=raw,cache=none -m 512m -cpu pentium3 -no-shutdown -no-reboot -monitor stdio;

#qemu -enable-kvm -device isa-debug-exit -machine pc -vga cirrus -drive file=../hdd.img,if=ide,index=0,media=disk,format=raw,cache=none -cpu qemu32 -no-shutdown -no-reboot -monitor stdio
qemu -enable-kvm -device isa-debug-exit -machine q35 -vga cirrus -drive file=../hdd.img,if=ide,index=0,media=disk,format=raw,cache=none -cpu qemu32 -monitor stdio #-no-reboot -no-shutdown



echo ""
#qemu -drive file=boot.bin,index=0,media=disk,format=raw -m 240 -monitor 127.0.0.1:55555,server,nowait;
#qemu-system-i386 --enable-kvm -drive file=image.img,index=0,media=disk,format=raw -m 512MB -cpu qemu32 -monitor telnet:127.0.0.1:55555,server,nowait;
