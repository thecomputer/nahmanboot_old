;Copyright 2019 Liav Albani

;Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

;The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

BITS 16
ORG 0x600

start:
  cli                         ; We do not want to be interrupted
  xor ax, ax                  ; 0 AX
  mov ds, ax                  ; Set Data Segment to 0
  mov es, ax                  ; Set Extra Segment to 0
  mov ss, ax                  ; Set Stack Segment to 0
  mov sp, ax                  ; Set Stack Pointer to 0

  ;;CopyLower
    mov cx, 0x0100            ; 256 WORDs in MBR
    mov si, 0x7C00            ; Current MBR Address
    mov di, 0x0600            ; New MBR Address
    rep movsw                 ; Copy MBR
	
  jmp 0:LowStart             ; Jump to new Address
 
LowStart:
;  sti                         ; Start interrupts
cli

;;CheckPartitions           ; Check Partition Table For Bootable Partition
;    mov bx, 0x1BE              ; Base = Partition Table Entry 1
;    mov cx, 4                 ; There are 4 Partition Table Entries
;.CKPTloop:
;      mov al, BYTE [bx+0x600]       ; Get Boot indicator bit flag
;      test al, 0x80           ; Check For Active Bit
;      jnz .CKPTFound          ; We Found an Active Partition
;      add bx, 0x10            ; Partition Table Entry is 16 Bytes
;      dec cx                  ; Decrement Counter
      
;      loop .CKPTloop           ; Loop

;	mov ax,0xDEAD
;    jmp ERROR                 ; ERROR!

;.CKPTFound:
;      mov cx, bx    ; Save Offset
;      add bx, 8               ; Increment Base to LBA Address

;; ReadVBR
;    mov ebx, [bx+0x600]       ; Start LBA of Active Partition

;    mov di, 0x7C00            ; We Are Loading VBR to 0x07C0:0x0000
;    mov cx, 1                 ; Only one sector
xor ebx,ebx	
mov bx,word [default_bios_boot_lba]
    call ReadSectors          ; Read Sector

;; jumpToVBR
;    ;cmp WORD [0x7DFE], 0xAA55 ; Check Boot Signature
;    ;jne ERROR                 ; Error if not Boot Signature
;   ;mov si,0x600
    ;add si,0x1be ; Entry to first partition...
;jmp 0:$    

	jmp 0x0:0x800                ; Jump To VBR


;; Defining Disk Address Packet Structure
daps1 db 10h
db 0 ; always 0
daps1_sectors dw 25 ; number of sectors to load - 4
daps1_offset dw 0x800 ; offset to transfer
daps1_segment dw 0x0 ; segment to transfer
daps1_lba dq 0 ; lower 32 bits of 48Bit LBA
default_bios_boot_lba dw 0x800
ReadSectors:
    lea si, [daps1]
    mov [daps1_lba],ebx
    call load_data_from_disk
    ret

; Function: load_data_from_disk
; Parameters: no parameters
; Purpose: Load Data from Disk
; Returns: nothing
load_data_from_disk:
    push ax
    push dx
    mov ah,0x42
    mov dl,0x80
    int 13h
    pop dx
    pop ax
ret

	ERROR:
		mov ecx,0xDEADC0DE
		jmp $


times (218 - ($-$$)) nop      ; Pad for disk time stamp
 
DiskTimeStamp times 8 db 0    ; Disk Time Stamp
 
bootDrive db 0                ; Our Drive Number Variable
PToff dw 0                    ; Our Partition Table Entry Offset

times (0x1b4 - ($-$$)) nop    ; Pad For MBR Partition Table