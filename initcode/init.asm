;Copyright 2019 Liav Albani

;Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

;The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

BITS 32
ORG 0x15000
;; Assembly written init ramdisk!

jmp init_start

delay_cpu:
push ecx

mov ecx,30000
	loop_wait1:
		push ecx
		mov ecx,3000
			loop_wait2:
			nop
			loop loop_wait2
		nop
		pop ecx

	loop loop_wait1

pop ecx
ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

init_start:
cli
	mov bx,0xDEAD


	;call Beep

	mov esi,ebx

	mov ebx,0xDEADC0DE
	mov ecx,ebx

	;mov esi,0x40000
	;call scan_pci_devices


	;mov esi,0x40000
	;push esi
	;call print_found_pci

	mov ebx,[0x10000+40]
	;xor esi,esi
	;mov si,word [0x10000+2+2+4*2+4]
	;mov byte [ebx],7h

	mov dh,6
	mov dl,9
	mov al,11

	;mov dh,255
	;mov dl,77
	;mov al,231
	
		push ecx
    	push ebx
    	mov ecx,800*600
		mov ecx,1024*768
    	loop_write_pixels1:
       	
		mov byte [ebx],al
        	inc ebx
		mov byte [ebx],dl
		inc ebx 
		mov byte [ebx],dh
		inc ebx 
		
        	loop loop_write_pixels1
    
	
    	pop ebx 
    	pop ecx


	



	;call delay_cpu



	;call delay_cpu

;; init PIT


;; init Keyboard

;;;;;in al,0x60

;;;;;;;;;;;;;;

;;;;;;;;;;;;;;



;lea ebx,[zero]

;mov dx,0x64
;.loop_wait_for_test5:
;	in al,dx
;	and al,01b
;	cmp al,01b
;	jne .loop_wait_for_test5
;mov dx,0x60
;	in al,dx
;	mov ah,al
;	;add al,32
;	in al,dx
;	mov byte [string7],al
	


jmp after_idtr

idt:

TIMES 256*8 db 0

idtr:

dw 256*8-1
dd idt

after_idtr:


lidt [idtr]
    lea eax,[double_fault_isr]
    mov [idt+8*8],ax
    mov word [idt+8*8+2],0x8
    mov byte [idt+8*8+4],0
    mov byte [idt+8*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+8*8+6],ax
    
    lea eax,[keyboard_isr]
    mov [idt+0x21*8],ax
    mov word [idt+0x21*8+2],0x8
    mov byte [idt+0x21*8+4],0
    mov byte [idt+0x21*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x21*8+6],ax

    lea eax,[pit_isr]
    mov [idt+0x20*8],ax
    mov word [idt+0x20*8+2],0x8
    mov byte [idt+0x20*8+4],0
    mov byte [idt+0x20*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x20*8+6],ax

    lea eax,[video_isr]
    mov [idt+0x49*8],ax
    mov word [idt+0x49*8+2],0x8
    mov byte [idt+0x49*8+4],0
    mov byte [idt+0x49*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x49*8+6],ax

    lea eax,[sata_disk_isr]
    mov [idt+0x59*8],ax
    mov word [idt+0x59*8+2],0x8
    mov byte [idt+0x59*8+4],0
    mov byte [idt+0x59*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x59*8+6],ax

    lea eax,[pci_isr]
    mov [idt+0x60*8],ax
    mov word [idt+0x60*8+2],0x8
    mov byte [idt+0x60*8+4],0
    mov byte [idt+0x60*8+5], 0x8E ;10001110b
    shr eax,16
    mov [idt+0x60*8+6],ax

	

    ;int 79

;; mask imr

mov dx,0x21
mov al,0xFD
out dx,al

in al,0x20
mov ah,al
in al,0xa0

%define init 0x11
%define init 0x11

%define offset1 0x20
%define offset2 0x28

%define slavepic 4
%define slavepic2 2

%define pic8086mode 0x1

push ax

mov al,init
mov dx,0x20
out dx,al
mov dx,0xa0
out dx,al

mov al,offset1
mov dx,0x21
out dx,al

mov al,offset2
mov dx,0xa1
out dx,al

mov al,slavepic
mov dx,0x21
out dx,al

mov al,slavepic2
mov dx,0xa1
out dx,al

mov al,pic8086mode
mov dx,0x21
out dx,al
mov dx,0xa1
out dx,al

pop ax

mov dx,0xa0
out dx,al
mov al,ah
mov dx,0x20
out dx,al

in al,0x60
in al,0x60




;sti ; time to restore interrupts...

;;  init video ISR vars

mov ebx,[0x10000+40]
mov [screen_buffer_addr],ebx

lea ebx,[arr]
mov [screen_font_addr],ebx

mov dl,'D'


lea ecx,[string]
mov ax,0x0

int 0x49

;mov bl,'!'
;mov ax,7
;int 0x49

;mov ecx,0xbabedead
;	hlt


lea ecx,[string2]
mov ax,0x0

int 0x49



;lea ecx,[string3]
;mov ax,0x0

;int 0x49



;lea ecx,[string4]
;mov ax,0x0

;int 0x49

;lea ecx,[string5]
;mov ax,0x0

;int 0x49

;lea ecx,[string6]
;mov ax,0x0

;int 0x49

lea ecx,[string7]
;mov ax,0x0

;int 0x49

;lea ecx,[string8]
;mov ax,0x0

;int 0x49

;lea ecx,[string9]
;mov ax,0x0

;int 0x49

;;;;;;; init PS/2 Controller & Keyboard


mov dx,0x64
mov al,0xAD
out dx,al
mov al,0xA7
out dx,al

mov dx,0x60
in al,dx



mov dx,0x64
mov al,0x20
out dx,al

;in al,0x60
;jmp 0x8:$
;and al,1110001b

;push ax

;mov dx,0x64
;mov al,0x60
;out dx,al

;pop ax

mov dx,0x64
mov al,0x60
out dx,al

mov al,1110001b
mov dx,0x60
out dx,al

mov dx,0x64
mov al,0xAA
out dx,al

mov dx,0x60

.loop_wait_for_selftest:
	in al,dx
	cmp al,0x55
	jne .loop_wait_for_selftest

mov dx,0x64
mov al,0xAB
out dx,al

mov dx,0x60
.loop_wait_for_test1:
	in al,dx
	cmp al,0x0
	jne .loop_wait_for_test1


mov dx,0x64
mov al,0xA9
out dx,al

mov dx,0x60
.loop_wait_for_test2:
	in al,dx
	cmp al,0x0
	jne .loop_wait_for_test2

mov dx,0x64
mov al,0xAE
out dx,al

mov dx,0x64
mov al,0xA8
out dx,al

mov dx,0x64
mov al,0xF5
out dx,al

sti

hlt
;mov bh,0
;mov bl,5
;mov al,0
;mov ah,0


mov edi,0x11000 ; initial offset
mov cx,1
int 0x60


;BH=bus number,BL=Device Number,AH=Function number,AL=register_offset
mov edx,0xf00000
mov bh,0
mov bl,1
mov al,0x20
mov ah,1
mov cx,2
int 0x60

;mov edx,0xf000000
;mov bh,0
;mov bl,2
;mov al,0x10
;mov ah,0
;mov cx,2
;int 0x60

mov ax,0
int 0x59

; read DMA SATA, EBP = Address of buffer, ECX = LBA Address, DL = PORT (Of Storage device), DH = Number of sectors
mov ax,2
mov ecx,0
mov ebp,0x10000
int 0x59

lea ecx,[string9]
mov ax,0x0

int 0x49

;mov ax,3
;int 0x59

;lea ecx,[string9]
;mov ax,0x0

;int 0x49

mov ax,2
mov ecx,1
mov ebp,0xB00
int 0x59

lea ecx,[string9]
mov ax,0x0
int 0x49

mov ax,2
mov ecx,2
mov ebp,0x2000
int 0x59


;cli
;hlt


;lea ecx,[string9]
;mov ax,0x0

;int 0x49

;mov bl,'!'
;mov ax,7
;int 0x49

;jmp 0x8:$

;int 21h

halt:
jmp halt
jmp halt


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;; ISRs & Handlers                        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


double_fault_isr:

	;call clear_screen

	lea ecx,[string_double_fault]
	mov ax,0x0
	int 0x49

	mov ecx,0xdeadbeef
	mov edx,0xcafebabe
	cli
	hlt

;; About PCI info structure
;; formed as a simple linked list

;; each node structured like so (for each device, and each function of it):
;;; | Next node (64 Bit Address) | Size of current node (16 Bit Value) | 32 Bit Reserved |
;;; | Bus Number (16 Bit Value)  | Device Number (16 Bit value)	 | Function num (16 bit) |
;;; | Header type (8 Bit value)  | Reserved (64 bit Address)	 			 |
;;; | Type Device (32 Bit value) | Activated IRQ (16 Bit value)        | Flags (32 Bit)  |
;;; |  50 Byte Reserved for later use							 |
 

;;; Flags 	|    0      |      1-31     |
;;;		| Activated |    Reserved   |

;;; Notes:
;;; 1. When next_node value equals = 0x7FFF,FFFF,FFFF,FFFF it means no node comes after the current node in the linked list
;;; 2. If next_node equals 0x0, A General Error should be raised as this value is wrong. The kernel should enumerate the PCI buses again.
;;; 3. Type Device conatins Class Code, Sub class, Prog IF & revision ID
;;; 4. Typical size of node is 64 Byte Array

;;; Notes about flags:
;;; 1. Flag value = 0x0: True, else false
;;; 2. By default, no virtual devices should appear on system. However, some modules can in fact install a node with virtual PCI device, for some sorts of operations.
;;; 3. Virtual flag being true (0x1 bit) means Type Device must be non 0x0, Header type value = (MF = 0, non multiple function device), Header type = 0x0
;;; Virtual Function Memory Address must contain a valid (non zero and less than 0x7FFF,FFFF,FFFF,FFFF) address to a obligated module.

;;; A note about virtual PCI device: This kind of feature is special only in this OS. It supposed to let a tremendous flexibility in functionality of the OS.

pci_isr:

	jmp pci_isr_start
	
	linked_pointer dd 0
	linked_pointer2 dd 0

	memory_pointer1 dd 0
	memory_pointer2 dd 0

	access_method db 0 ; 0 = legacy (ports), 1 = memory mapped
	
	pci_isr_start:	

	cmp cx,0 ; read register, BH=bus number,BL=Device Number,AH=Function number,AL=register_offset,EDI=address | output on EDX
	je .pci_read_register
	cmp cx,1 ; pci bus enumeration
	je .pci_enumeration
	cmp cx,2 ; write register BH=bus number,BL=Device Number,AH=Function number,AL=register_offset,EDI=address | input on EDI
	je .pci_write_register

	jmp end_pci_isr

.pci_read_register:
	call pciConfig_read_register

	jmp end_pci_isr

.pci_write_register:
	call pciConfig_write_register

	jmp end_pci_isr

.pci_enumeration:
	call pciConfigEnumeration

	jmp end_pci_isr

end_pci_isr:
	iretd

;; input - void, output on EDX
pciConfig_read_register:
	push ebx
	push ecx
	;push edx
	push eax
	push edi


	
	;xor ebx,ebx
	;xor eax,eax

	xor ecx,ecx

	mov ch,bh
	shr ecx,3
	or cl,bl
	shl ecx,3
	or cl,ah
	
	shl ecx,8
	mov cl,al

	;mov cl,bh
	;shl ecx,16
	
	;shr bl,3
	;or bl,al


	;mov ch,bl
	;mov cl,al
	


	or ecx,10000000000000000000000000000000b ; set enable bit
	mov eax,ecx	

		mov dx,0xcf8
		out dx,eax

		push eax

		mov dx,0xcfc
		in eax,dx

		mov edx,eax

		pop eax	


	pop edi
	pop eax
	;pop edx
	pop ecx
	pop ebx
	ret


;; input in EDX, output void
pciConfig_write_register:
	push ebx
	push ecx
	push edx
	push eax
	push edi


	
	;xor ebx,ebx
	;xor eax,eax

	xor ecx,ecx

	mov ch,bh
	shr ecx,3
	or cl,bl
	shl ecx,3
	or cl,ah
	
	shl ecx,8
	mov cl,al

	;mov cl,bh
	;shl ecx,16
	
	;shr bl,3
	;or bl,al


	;mov ch,bl
	;mov cl,al
	


	or ecx,10000000000000000000000000000000b ; set enable bit
	mov eax,ecx	

		push edx
		mov dx,0xcf8
		out dx,eax
		pop edx

		push eax

		mov eax,edx
		
		mov dx,0xcfc
		out dx,eax

		

		pop eax	


	pop edi
	pop eax
	pop edx
	pop ecx
	pop ebx
	ret

	

; read register, BH=bus number,BL=Device Number,AH=Function number,AL=register_offset,EDI=address | output on EDX
;; Each entry (11 bytes): | Bus Number (1 byte) | Device Number (1 byte) | Function Number (1 byte) | vendor & device id (4 bytes) | class & ProgIF (4 bytes)
pciConfigEnumeration:
push ebx
push ecx
push edx
push eax
push edi

mov ecx,100 ; 256 buses

xor ebx,ebx
xor eax,eax

.loop_enumerate_pci_bus: 
push ecx
	mov ecx,32 ; 32 devices per bus	
	.loop_enumerate_pci_device: 
	
	xor ah,ah
	push ecx 
	mov ecx,7 ; 7 functions per 

		.loop_enumerate_pci_device_function:
		
		
		push ecx
			mov al,0
			mov cx,0
			int 0x60
					
		pop ecx
	
		cmp edx,0xffffffff
		je .dont_add_to_edi
		
		mov byte [edi],bh
		mov byte [edi+1],bl
		mov byte [edi+2],ah
		mov dword [edi+3],edx
		xor edx,edx
		push ecx
		
			mov al,8
			mov cx,0
			int 0x60
					
		pop ecx

		mov dword [edi+7],edx		

		add edi,11

		.dont_add_to_edi:
		inc ah
		loop .loop_enumerate_pci_device_function

	pop ecx

	inc bl
	loop .loop_enumerate_pci_device

pop ecx

	inc bh		
loop .loop_enumerate_pci_bus

pop edi
pop eax
pop edx
pop ecx
pop ebx

ret





fs_isr:

	iretd

%define PCI_CLASSCODE_SATA 0x0106

%define	SATA_SIG_ATA	0x00000101	// SATA drive
%define	SATA_SIG_ATAPI	0xEB140101	// SATAPI drive
%define	SATA_SIG_SEMB	0xC33C0101	// Enclosure management bridge
%define	SATA_SIG_PM	0x96690101	// Port multiplier
 
%define AHCI_DEV_NULL 0
%define AHCI_DEV_SATA 1
%define AHCI_DEV_SEMB 2
%define AHCI_DEV_PM 3
%define AHCI_DEV_SATAPI 4
 
%define HBA_PORT_IPM_ACTIVE 1
%define HBA_PORT_DET_PRESENT 3


%define FIS_TYPE_REG_H2D	 0x27	// Register FIS - host to device
%define FIS_TYPE_REG_D2H	 0x34	// Register FIS - device to host
%define FIS_TYPE_DMA_ACT	 0x39	// DMA activate FIS - device to host
%define FIS_TYPE_DMA_SETUP	 0x41	// DMA setup FIS - bidirectional
%define FIS_TYPE_DATA		 0x46	// Data FIS - bidirectional
%define FIS_TYPE_BIST		 0x58	// BIST activate FIS - bidirectional
%define FIS_TYPE_PIO_SETUP	 0x5F	// PIO setup FIS - device to host
%define FIS_TYPE_DEV_BITS	 0xA1	// Set device bits FIS - device to host

%define ATA_CMD_READ_DMA 0xC8
%define ATA_CMD_READ_DMA_EX 0x25
%define ATA_CMD_WRITE_DMA 0xCA
%define ATA_CMD_WRITE_DMA_EX 0x35


sata_disk_isr:

	
	jmp sata_disk_isr_start

	.sata_io_lock db 1
	.sata_disk_lock db 0
	.pci_sata_bar5_addr dd 0
	.pci_sata_vendor_id dw 0
	.pci_sata_device_id dw 0
	.pci_sata_bus_number db 0
	.pci_sata_device_number db 0
	.pci_sata_function_number db 0

	;; SATA AHCI specific structures

	.disk_selected db 0
	.prdt_tables_pointer dd 0

	sata_HBA_port:
	.clb dd 0 ; command list down
	.clbu dd 0 ; command list up
	.fb dd 0 ; FIS base down
	.fbu dd 0 ; FIS base up
	.ie dd 0 ; interrupt enable
	.cmd dd 0 ; command
	.tfd dd 0 ; task file
	.sig dd 0 ; signature
	.ssts dd 0 ; sata status
	.sctl dd 0 ; sata control
	.serr dd 0 ; sata error
	.sact dd 0 ; sata active
	.ci dd 0 ; command issue
	.sntf dd 0 ; sata notification

	.counter db 0

	sata_disk_isr_start:

	cmp ax,0 ; initialize SATA controller
	je .init_controller

	cmp ax,1 ; check SATA driver (lock) state
	je .check_lock_state

	cmp ax,2 ; read DMA SATA, EBP = Address of buffer, ECX = LBA Address, DL = PORT (Of Storage device), DH = Number of sectors
	je .read_DMA_SATA

	cmp ax,3 ; clear interrupt status
	je .clear_interrupt_status

	;cmp byte [sata_disk_isr.sata_io_lock],1
	;je .sata_io_lock_exit

	

	jmp .end_sata_disk_isr

.sata_io_lock_exit:
	jmp .end_sata_disk_isr

.check_lock_state:
	jmp .end_sata_disk_isr

.init_controller:
	
	call initSATAcontroller ; input - EDI = address of PCI devices list
	jmp .end_sata_disk_isr

.read_DMA_SATA:

	call read_SATA_DMA
	jmp .end_sata_disk_isr

.clear_interrupt_status:

	call clearSATA_interrupt_status
	jmp .end_sata_disk_isr

.end_sata_disk_isr:	
	iretd

clearSATA_interrupt_status:

	push eax
	push ebx
	push ecx
	push edx

	mov ebx,[sata_disk_isr.pci_sata_bar5_addr]
	add ebx,0x100
	add ebx,0x10
	mov ebx,0xfebd5000+0x100+0x10
	mov dword [ebx],0
	
	mov ebx,0xfebd5000+0x100+0x18

	and dword [ebx],11111111111111111111111111111110b


	pop edx
	pop ecx
	pop ebx
	pop eax
	ret
initSATAcontroller:
	push eax
	push ebx
	push ecx
	push edx
	; loop through the PCI devices list, locating the entry of IDE controller
	
	.search_pci_devices:

		cmp dword [edi+3],0
		je .exit_init ; not found

		cmp word [edi+9], PCI_CLASSCODE_SATA
		je .exit_search_pci_devices		

		add edi,11

	jmp .search_pci_devices

	; setup values

	.exit_search_pci_devices:

	

	mov al,[edi]
	mov byte [sata_disk_isr.pci_sata_bus_number],al
	mov al,[edi+1]
	mov byte [sata_disk_isr.pci_sata_device_number],al
	mov al,[edi+2]
	mov byte [sata_disk_isr.pci_sata_function_number],al
	
	mov edx,[edi+3] ; get vendor ID & class ID
	mov dword [sata_disk_isr.pci_sata_vendor_id],edx


	lea ebx,[sata_disk_isr.sata_io_lock]

	

	; get BAR5 register (address of AHCI)

	;BH=bus number,BL=Device Number,AH=Function number,AL=register_offset
	
	mov bh,byte [sata_disk_isr.pci_sata_bus_number]
	mov bl,byte [sata_disk_isr.pci_sata_device_number]
	mov al,0x24 ; offset bar5
	mov ah,byte [sata_disk_isr.pci_sata_function_number]
	mov cx,0 ; read register, output on EDX
	int 0x60

	mov dword [sata_disk_isr.pci_sata_bar5_addr],edx

	;; setup default BARs for PIO (IDE Mode)

	mov edx,0x1F0
	mov al,0x10 ; offset bar0
	mov cx,2
	int 0x60
	mov edx,0x3F6
	mov al,0x14 ; offset bar1
	mov cx,2
	int 0x60
	mov edx,0x170
	mov al,0x18 ; offset bar2
	mov cx,2
	int 0x60
	mov edx,0x376
	mov al,0x1C ; offset bar3
	mov cx,2
	int 0x60

	;; setup BAR4 for DMA (IDE Mode)

	mov edx,0xd00
	mov al,0x20 ; offset bar4
	mov cx,2
	int 0x60

	
	; release lock
	mov byte [sata_disk_isr.sata_io_lock],0

	.exit_init:

	pop edx
	pop ecx
	pop ebx
	pop eax
	ret

; read DMA SATA, EBP = Address of buffer, ECX = LBA Address, DL = PORT (Of Storage device), DH = Number of sectors
read_SATA_DMA:

	push eax
	push ebx
	push ecx
	push edx
	push ebp


	;mov ebp,edx ; saving lba address in ebp!
	; set lock
	mov byte [sata_disk_isr.sata_io_lock],0

	; setup CMD HEADER

	mov ebx,[sata_disk_isr.pci_sata_bar5_addr]
	add ebx,0x100
	mov ebx,[ebx] ; command list base address, slot 0
	
push eax
push ebx
push ecx
push edx
push ebp

	;cmp byte [sata_HBA_port.counter],0
	;je .continue

	;push ecx
	;.loop_add_slot:

	;add ebx,0x20 ; each slot is 0x20 bytes wide
;
	;loop .loop_add_slot

	;pop ecx

	.continue:


	mov byte [ebx], 5 ; cfl = 5 dword
	inc ebx ; next byte
	mov byte [ebx], 0 ; just zero it
	inc ebx ; moving to prdtl
	mov byte [ebx],1 ; one entry in prdt!
	inc ebx ; next byte
	inc ebx ; next byte, moving to next dword in command
	
	mov dword [ebx],512 ; 512 byte transfer!
	add ebx,4 ; next dword!
	mov dword [ebx],0xA000 ; command table address
	add ebx,4 ; next dword!
	mov dword [ebx],0 ; upper 32bit address = 0	

pop ebp
pop edx
pop ecx
pop ebx
pop eax
	; setup Command Table & FIS

	mov ebx,0xA000 ; setup FIS first!
push eax
push ebx
push ecx
push edx
push ebp

	mov byte [ebx], 0x27 ; FIS_TYPE_REG_H2D ; fis type
	inc ebx
	mov al,0
	or al,10000000b
	mov byte [ebx],al ; command = 1
	inc ebx

	mov byte [ebx], ATA_CMD_READ_DMA_EX ; command register

	inc ebx

	mov byte [ebx],0 ;  feature register low

	inc ebx

	; LBA registers

	

	mov byte [ebx],cl ; LBA #1
	inc ebx
	mov byte [ebx],ch ; LBA #2
	inc ebx
	shr ecx,16
	mov byte [ebx],cl ; LBA #3
	inc ebx
	mov byte [ebx],1<<6 ; device register (LBA mode)

	inc ebx

	; LBA registers #2
	mov byte [ebx],ch ; LBA #4
	inc ebx
	mov byte [ebx],0 ; LBA #5
	inc ebx
	mov byte [ebx],0 ; LBA #6
	inc ebx
	mov byte [ebx],0 ; feature register high

	inc ebx

	; DWORD 3 registers
	mov byte [ebx],1 ; Count low
	inc ebx
	mov byte [ebx],0 ; Count high
	inc ebx
	mov byte [ebx],0 ; icc
	inc ebx
	mov byte [ebx],0 ; control register

	inc ebx


	; DWORD 4 registers - reserved
	mov byte [ebx],0 
	inc ebx
	mov byte [ebx],0 
	inc ebx
	mov byte [ebx],0 
	inc ebx
	mov byte [ebx],0 



pop ebp
pop edx
pop ecx
pop ebx
pop eax

	add ebx,0x80 ; prdt region!

push ebx

	mov dword [ebx], ebp ; EBP = Address of buffer ;0xB000
	add ebx,4
	mov dword [ebx],0
	add ebx,8
	mov eax,10000000000000000000000000000000b
	or eax,0x200
	;or eax,0x1
	mov dword [ebx],eax


pop ebx


	; execute!!

	mov ebx,[sata_disk_isr.pci_sata_bar5_addr]
	add ebx,0x100

	add ebx,0x38

	mov byte [ebx],1

	.wait_complete:
	cmp dword [ebx],0
	jne .wait_complete

	; release lock
	mov byte [sata_disk_isr.sata_io_lock],0

	.exit_init:

	inc byte [sata_HBA_port.counter]

	;cli

	pop ebp
	pop edx
	pop ecx
	pop ebx
	pop eax

ret

IRQ15_isr:

IRQ14_isr:
	





keyboard_buffer db 0
times 100 db 0

keyboard_isr:

	push eax
	push ebx

	mov ebx,0xdeadc0de
	in al,0x60
	mov ah,al
	in al,0x60

	;call Beep

	;lea ecx,[string9]
	;mov ax,0x0
	;int 0x49

push ax
push dx
mov al,0x20
mov dx,0x20
out dx,al
pop dx
pop ax
;sti

pop ebx
pop eax
iretd



video_isr:

	jmp start_video_isr

	row_number db 0
	column_number db 0
	screen_buffer_addr dd 0
	screen_font_addr dd 0
	screen_font_width db 8
	screen_font_height db 16
	row_pixels_number dd 768*4*15
	row_pixels_line_number dd 768*4

	character_print db 0,0

	start_video_isr:

	cmp ax,0 ; print string
	je .isr_pring_string

	cmp ax,1 ; print string fault
	je .isr_pring_string_fault

	cmp ax,2 ; clear_line
	je .isr_clear_line

	;cmp ax,3 ; transfer data from other framebuffer

	;cmp ax,4 ; clear_screen

	cmp ax,5 ; change font address , ebx = address of new font array
	je .isr_change_font
	cmp ax,6 ; change framebuffer address , ebx = address of framebuffer
	je .isr_change_framebuffer_addr

	cmp ax,7 ; print char, bl = ascii number of character
	je .isr_pring_char

	;cmp ax,8 ; delete char
	;je .isr_pring_char

	;jmp end_video_isr

	jmp end_video_isr

.isr_pring_string:
	
	;call clear_line
	mov byte [column_number],0
	call print_string_isr2

	jmp end_video_isr

.isr_pring_char:
	
	;call clear_line

	;mov byte [column_number],55
	;mov byte [row_number],20

	push ecx

	mov byte [character_print],bl
	lea ecx,[character_print] ; get address of "new string"
	call print_char_isr
	
	pop ecx
	inc byte [column_number]

	jmp end_video_isr

.isr_pring_string_fault:
	mov byte [column_number],0
	call clear_screen
	mov byte [row_number],0
	call print_string_isr2
	jmp end_video_isr

.isr_clear_line:
	call clear_line
	jmp end_video_isr	

.isr_change_font:
	mov [screen_font_addr],ebx
	jmp end_video_isr

.isr_change_framebuffer_addr:
	mov [screen_buffer_addr],ebx
	jmp end_video_isr

end_video_isr:

	push ax
	push dx
	mov al,0x20
	out 0x20,al
	pop dx
	pop ax
	iretd

;;; ecx - addr of string
;;; returns void
print_string_isr2:
push ebp
push ecx
push edi
push ebx 
push eax


mov ebp,edi

	
	;jmp cont_no_zeroing_row

	
.loop_print_string2:
	
	cmp word [ecx], "\n"
	jne .cont_printing2

	inc byte [row_number]
	mov byte [column_number],0

	add ecx,2

	jmp .reloop_print2

	.cont_printing2:

	;; calling itself!
	
	mov bl,byte [ecx]
	mov ax,7
	int 0x49
	inc ecx	
	
	cmp byte [column_number],113
	jne .cont_no_inc_row

	inc byte [row_number]
	mov byte [column_number],0
	
	.reloop_print2:

	.cont_no_inc_row:
	
	cmp byte [ecx],0
	jne .loop_print_string2

inc byte [row_number]

cmp byte [row_number],50
jng cont_no_zeroing_row

mov byte [row_number],0

cont_no_zeroing_row:

pop eax
pop ebx
pop edi
pop ecx
pop ebp
ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ecx - address of character
;; returns void

print_char_isr:
push ebp
push ecx
push edi
push ebx 
push eax

mov edi,[screen_buffer_addr]
mov ebx,[screen_font_addr]

add ebx,32

push ecx
push ebx

	xor ebx,ebx

	mov bl, byte [row_number]
	cmp bl,0
	je .exit_loop_choose_row
	
	mov ecx,ebx

	.loop_choose_row:

		add edi,dword [row_pixels_number]

	loop .loop_choose_row

	.exit_loop_choose_row:
	
	xor ecx,ecx

	;mov byte [column_number],

	xor ebx,ebx
	
	mov bl, byte [column_number]
	cmp bl,0
	je .exit_loop_choose_col
	
	mov ecx,ebx

	je .exit_loop_choose_col

	.loop_choose_col:

		add edi,9*3

	loop .loop_choose_col

	.exit_loop_choose_col:

	add edi,3
	

pop ebx
pop ecx

;add edi,3*2

push edx
push ecx

	xor edx,edx
	mov eax,16
	mov cl,byte [ecx]
	mul cl		

pop ecx	
pop edx

	add ebx,eax
	call print_char

pop eax
pop ebx
pop edi
pop ecx
pop ebp
ret



;;; ecx - number_line
;;; returns void
clear_line:
push ecx
push edi
push ebx 
push eax
push edx

mov edi,[screen_buffer_addr]

push ecx

	mov ecx,[row_number]
	cmp ecx,0
	je exit_loop_choose_row2

	loop_choose_row2:

		add edi,768*4*15

	loop loop_choose_row2
	;sub edi,768*4*15
	exit_loop_choose_row2:

	

pop ecx



	mov dh,6
	mov dl,9
	mov al,11
	
	push ecx
    	push ebx

	
	mov ecx,768*4*10

    	.loop_write_pixels2:
        	
		mov byte [edi],al
        	inc edi
		mov byte [edi],dl
		inc edi 
		mov byte [edi],dh
		inc edi

		;add edi,768

        	loop .loop_write_pixels2
    

    	pop ebx 
    	pop ecx
	
pop edx
pop eax
pop ebx
pop edi
pop ecx
ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;

pit_isr:
cli
push ax
push dx
mov al,0x20
out 0x20,al
pop dx
pop ax



;mov ebx,0xdeadcafe
sti
iret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;; need to implement, partly stub for now

acpi_enable_apic:
	; detecting rsdp

		;; trying to search "RSD PTR " from 0xE0000 to 0xFFFFF
	
		mov ebx,0xE0000
		mov ecx, 8191
		;lea edi,[checksum]		

		.loop_searching_checksum:
		
			push ecx
			push ebx
			push edi

			mov ecx,8
			.loop_compare_string:
				mov dl,byte [edi]
				cmp [ebx],dl
				jne .exit_loop_compare_string
				inc edi
				inc ebx
			loop .loop_compare_string
			jmp .exit_loop_finding_ebx		

			.exit_loop_compare_string:

			pop edi
			pop ebx
			pop ecx
			add ebx,0x10	
		loop .loop_searching_checksum

		;; trying to search "RSD PTR " in EBDA

	.exit_loop_finding_ebx:

			pop edi
			pop ebx
			pop ecx
	
	;; ebx contains address of RSDP

	;;; check revision
	mov dl, byte [ebx+15] ; revision
	cmp dl,0 ; ACPI 1.0
	ja .exit_acpi 

	;;; validating table (important!)

	mov dl, byte [ebx+8] ; checksum!

	jmp .exit_acpi ; didn't work, abort

	




	.exit_acpi:
	ret


;;;;;;;;;;;;;

;;;;;;;;;;;;
print_char:

push ebp
mov ebp,esp
push eax
push ebx
push edx
push ecx
push edi


	xor ecx,ecx
	mov cl, 16; byte [screen_font_height]

loop_print_char_all:

push ecx
push eax
push edi
	xor ecx,ecx
	mov cl, byte [screen_font_width]
	rol al,4
	mov al,byte [ebx]

	push ecx

	xor ecx,ecx
	mov cl, byte [screen_font_width] ; for example: 8 pixels width - 1
	dec ecx

	.loop_inc_edi:
		add edi,3
	loop .loop_inc_edi

	pop ecx

	;add edi,7*3
loop_print_char:
	push edi
	mov ah,0x1
	and ah,al
	cmp ah,0
	je continue_printing_char
	
	push edx
	push eax
	
	mov dh,255
	mov dl,255
	mov al,255
	mov byte [edi],dh
	inc edi
	mov byte [edi],dl
	inc edi
	mov byte [edi],al
	inc edi
	
	pop eax
	pop edx

	continue_printing_char:
	pop edi
	shr al,1
	sub edi,3
	
	loop loop_print_char

	pop edi
	pop eax
	pop ecx

	add edi, [row_pixels_line_number] ;esi
	add ebx,1

	loop loop_print_char_all


pop edi
pop ecx
pop edx
pop ebx
pop eax
pop ebp
	ret

clear_screen:
	push ebx
	push esi
	push eax
	push edx
	push ecx
	
	mov ebx,[0x10000+40]
	xor esi,esi
	mov si,word [0x10000+2+2+4*2+4]
	;mov byte [ebx],7h

	mov dh,6
	mov dl,9
	mov al,11
	
	push ecx
    	push ebx
    	;mov ecx,800*600
	mov ecx,1024*768
    	.loop_write_pixels1:
        	
		mov byte [ebx],al
        	inc ebx
		mov byte [ebx],dl
		inc ebx 
		mov byte [ebx],dh
		inc ebx 
		
        	loop .loop_write_pixels1
    

    	pop ebx 
    	pop ecx

pop ecx
pop edx
pop eax
pop esi
pop ebx

ret

string_double_fault db "NahmanOS panic: fatal error, double faulting. Halting.",0

line_number db 0
string db  "[nahmanboot]: using 1024x768-vesa bgr (rgb reversed) color, defaults to 8x16 font\nNew Line!",0
string2 db "[nahmanboot]: vesa info block at 0x1000",0
string3 db "[nahmanboot]: bios-e820 memory map detected at 0x7500",0
string4 db "[nahmanboot]: probing pci, general devices list at 0x40000",0
string5 db "[nahmanboot]: using legacy io ports for probing pci - 0xcf8 for address, 0xcfc for data",0
string6 db "[nahmanboot]: A20 line enabled, protected mode enabled, PG disabled",0
string7 db "[nahmanboot]: idt structures & double fault handler initialized",0
string8 db "[nahmanboot]: continue initialization",0
string9 db "[nahmanboot]: keyboard irq 9 called...",0
;arr_custom db 00000000b,00000000b,00000000b,00010000b,00111000b,01101100b,11000110b,11000110b,11111110b,11000110b,11000110b,11000110b,11000110b,00000000b,00000000b,00000000b 
arr db 114,181,74,134,00,00,00,00,32,00,00,00,01,00,00,00,00,01,00,00,16,00,00,00,16,00,00,00,08,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,126,129,165,129,129,189,
db 153,129,129,126,00,00,00,00,00,00,126,255,219,255,255,195,231,255,255,126,00,00,00,00,00,00,00,00,108,254,254,254,254,124,56,16,00,00,00,00,00,00,00,00,16,56,124,254,124,56,16,00,00,00,00,00,00,00,00,24
db 60,60,231,231,231,24,24,60,00,00,00,00,00,00,00,24,60,126,255,255,126,24,24,60,00,00,00,00,00,00,00,00,00,00,24,60,60,24,00,00,00,00,00,00,255,255,255,255,255,255,231,195,195,231,255,255,255,255,255
db 255,00,00,00,00,00,60,102,66,66,102,60,00,00,00,00,00,255,255,255,255,255,195,153,189,189,153,195,255,255,255,255,255,00,00,30,14,26,50,120,204,204,204,204,120,00,00,00,00,00,00,60,102,102,102,102,60,24,126
db 24,24,00,00,00,00,00,00,63,51,63,48,48,48,48,112,240,224,00,00,00,00,00,00,127,99,127,99,99,99,99,103,231,230,192,00,00,00,00,00,00,24,24,219,60,231,60,219,24,24,00,00,00,00,00,128,192,224,240,248,254,248,240,224
db 192,128,00,00,00,00,00,02,06,14,30,62,254,62,30,14,06,02,00,00,00,00,00,00,24,60,126,24,24,24,126,60,24,00,00,00,00,00,00,00,102,102,102,102,102,102,102,00,102,102,00,00,00,00,00,00,127,219,219,219,123,27,27,27
db 27,27,00,00,00,00,00,124,198,96,56,108,198,198,108,56,12,198,124,00,00,00,00,00,00,00,00,00,00,00,254,254,254,254,00,00,00,00,00,00,24,60,126,24,24,24,126,60,24,126,00,00,00,00,00,00,24,60,126,24,24,24,24,24,24
db 24,00,00,00,00,00,00,24,24,24,24,24,24,24,126,60,24,00,00,00,00,00,00,00,00,00,24,12,254,12,24,00,00,00,00,00,00,00,00,00,00,00,48,96,254,96,48,00,00,00,00,00,00,00,00,00,00,00,00,192,192,192,254,00,00,00,00
db 00,00,00,00,00,00,00,36,102,255,102,36,00,00,00,00,00,00,00,00,00,00,16,56,56,124,124,254,254,00,00,00,00,00,00,00,00,00,254,254,124,124,56,56,16,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00 
db 24,60,60,60,24,24,24,00,24,24,00,00,00,00,00,102,102,102,36,00,00,00,00,00,00,00,00,00,00,00,00,00,00,108,108,254,108,108,108,254,108,108,00,00,00,00,24,24,124,198,194,192,124,06,06,134,198,124,24,24,00,00,00,00,00
db 00,194,198,12,24,48,96,198,134,00,00,00,00,00,00,56,108,108,56,118,220,204,204,204,118,00,00,00,00,00,48,48,48,96,00,00,00,00,00,00,00,00,00,00,00,00,00,12,24,48,48,48,48,48,48,24,12,00,00,00,00,00,00,48,24,12,12,12,12 
db 12,12,24,48,00,00,00,00,00,00,00,00,00,102,60,255,60,102,00,00,00,00,00,00,00,00,00,00,00,24,24,126,24,24,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,24,24,24,48,00,00,00,00,00,00,00,00,00,00,126,00,00,00,00,00,00,00,00,00
db 00,00,00,00,00,00,00,00,00,24,24,00,00,00,00,00,00,00,00,02,06,12,24,48,96,192,128,00,00,00,00,00,00,124,198,198,206,222,246,230,198,198,124,00,00,00,00,00,00,24,56,120,24,24,24,24,24,24,126,00,00,00,00,00,00,124,198,06,12,24
db 48,96,192,198,254,00,00,00,00,00,00,124,198,06,06,60,06,06,06,198,124,00,00,00,00,00,00,12,28,60,108,204,254,12,12,12,30,00,00,00,00,00,00,254,192,192,192,252,06,06,06,198,124,00,00,00,00,00,00,56,96,192,192,252,198,198,198
db 198,124,00,00,00,00,00,00,254,198,06,06,12,24,48,48,48,48,00,00,00,00,00,00,124,198,198,198,124,198,198,198,198,124,00,00,00,00,00,00,124,198,198,198,126,06,06,06,12,120,00,00,00,00,00,00,00,00,24,24,00,00,00,24,24,00,00,00
db 00,00,00,00,00,00,24,24,00,00,00,24,24,48,00,00,00,00,00,00,00,06,12,24,48,96,48,24,12,06,00,00,00,00,00,00,00,00,00,126,00,00,126,00,00,00,00,00,00,00,00,00,00,96,48,24,12,06,12,24,48,96,00,00,00,00,00,00,124,198,198,12,24,24
db 24,00,24,24,00,00,00,00,00,00,124,198,198,198,222,222,222,220,192,124,00,00,00,00,00,00,16,56,108,198,198,254,198,198,198,198,00,00,00,00,00,00,252,102,102,102,124,102,102,102,102,252,00,00,00,00,00,00,60,102,194,192,192,192
db 192,194,102,60,00,00,00,00,00,00,248,108,102,102,102,102,102,102,108,248,00,00,00,00,00,00,254,102,98,104,120,104,96,98,102,254,00,00,00,00,00,00,254,102,98,104,120,104,96,96,96,240,00,00,00,00,00,00,60,102,194,192,192,222,198
db 198,102,58,00,00,00,00,00,00,198,198,198,198,254,198,198,198,198,198,00,00,00,00,00,00,60,24,24,24,24,24,24,24,24,60,00,00,00,00,00,00,30,12,12,12,12,12,204,204,204,120,00,00,00,00,00,00,230,102,102,108,120,120,108,102,102,230
db 00,00,00,00,00,00,240,96,96,96,96,96,96,98,102,254,00,00,00,00,00,00,195,231,255,255,219,195,195,195,195,195,00,00,00,00,00,00,198,230,246,254,222,206,198,198,198,198,00,00,00,00,00,00,124,198,198,198,198,198,198,198,198,124
db 00,00,00,00,00,00,252,102,102,102,124,96,96,96,96,240,00,00,00,00,00,00,124,198,198,198,198,198,198,214,222,124,12,14,00,00,00,00,252,102,102,102,124,108,102,102,102,230,00,00,00,00,00,00,124,198,198,96,56,12,06,198,198,124
db 00,00,00,00,00,00,255,219,153,24,24,24,24,24,24,60,00,00,00,00,00,00,198,198,198,198,198,198,198,198,198,124,00,00,00,00,00,00,195,195,195,195,195,195,195,102,60,24,00,00,00,00,00,00,195,195,195,195,195,219,219,255,102,102
db 00,00,00,00,00,00,195,195,102,60,24,24,60,102,195,195,00,00,00,00,00,00,195,195,195,102,60,24,24,24,24,60,00,00,00,00,00,00,255,195,134,12,24,48,96,193,195,255,00,00,00,00,00,00,60,48,48,48,48,48,48,48,48,60,00,00,00,00,00
db 00,00,128,192,224,112,56,28,14,06,02,00,00,00,00,00,00,60,12,12,12,12,12,12,12,12,60,00,00,00,00,16,56,108,198,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,255,00,00,48,48,24,00,00,00,00,00
db 00,00,00,00,00,00,00,00,00,00,00,00,00,120,12,124,204,204,204,118,00,00,00,00,00,00,224,96,96,120,108,102,102,102,102,124,00,00,00,00,00,00,00,00,00,124,198,192,192,192,198,124,00,00,00,00,00,00,28,12,12,60,108,204,204
db 204,204,118,00,00,00,00,00,00,00,00,00,124,198,254,192,192,198,124,00,00,00,00,00,00,56,108,100,96,240,96,96,96,96,240,00,00,00,00,00,00,00,00,00,118,204,204,204,204,204,124,12,204,120,00,00,00,224,96,96,108,118,102,102
db 102,102,230,00,00,00,00,00,00,24,24,00,56,24,24,24,24,24,60,00,00,00,00,00,00,06,06,00,14,06,06,06,06,06,06,102,102,60,00,00,00,224,96,96,102,108,120,120,108,102,230,00,00,00,00,00,00,56,24,24,24,24,24,24,24,24,60,00,00
db 00,00,00,00,00,00,00,230,255,219,219,219,219,219,00,00,00,00,00,00,00,00,00,220,102,102,102,102,102,102,00,00,00,00,00,00,00,00,00,124,198,198,198,198,198,124,00,00,00,00,00,00,00,00,00,220,102,102,102,102,102,124,96,96
db 240,00,00,00,00,00,00,118,204,204,204,204,204,124,12,12,30,00,00,00,00,00,00,220,118,102,96,96,96,240,00,00,00,00,00,00,00,00,00,124,198,96,56,12,198,124,00,00,00,00,00,00,16,48,48,252,48,48,48,48,54,28,00,00,00,00,00,00
db 00,00,00,204,204,204,204,204,204,118,00,00,00,00,00,00,00,00,00,195,195,195,195,102,60,24,00,00,00,00,00,00,00,00,00,195,195,195,219,219,255,102,00,00,00,00,00,00,00,00,00,195,102,60,24,60,102,195,00,00,00,00,00,00,00,00
db 00,198,198,198,198,198,198,126,06,12,248,00,00,00,00,00,00,254,204,24,48,96,198,254,00,00,00,00,00,00,14,24,24,24,112,24,24,24,24,14,00,00,00,00,00,00,24,24,24,24,00,24,24,24,24,24,00,00,00,00,00,00,112,24,24,24,14,24,24,24
db 24,112,00,00,00,00,00,00,118,220,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,16,56,108,198,198,198,254,00,00,00,00,00,00,00,60,102,194,192,192,192,194,102,60,12,06,124,00,00,00,00,204,00,00,204,204,204,204,204,204,118
db 00,00,00,00,00,12,24,48,00,124,198,254,192,192,198,124,00,00,00,00,00,16,56,108,00,120,12,124,204,204,204,118,00,00,00,00,00,00,204,00,00,120,12,124,204,204,204,118,00,00,00,00,00,96,48,24,00,120,12,124,204,204,204,118,00
db 00,00,00,00,56,108,56,00,120,12,124,204,204,204,118,00,00,00,00,00,00,00,00,60,102,96,96,102,60,12,06,60,00,00,00,00,16,56,108,00,124,198,254,192,192,198,124,00,00,00,00,00,00,198,00,00,124,198,254,192,192,198,124,00,00,00
db 00,00,96,48,24,00,124,198,254,192,192,198,124,00,00,00,00,00,00,102,00,00,56,24,24,24,24,24,60,00,00,00,00,00,24,60,102,00,56,24,24,24,24,24,60,00,00,00,00,00,96,48,24,00,56,24,24,24,24,24,60,00,00,00,00,00,198,00,16,56,108
db 198,198,254,198,198,198,00,00,00,00,56,108,56,00,56,108,198,198,254,198,198,198,00,00,00,00,24,48,96,00,254,102,96,124,96,96,102,254,00,00,00,00,00,00,00,00,00,110,59,27,126,216,220,119,00,00,00,00,00,00,62,108,204,204,254
db 204,204,204,204,206,00,00,00,00,00,16,56,108,00,124,198,198,198,198,198,124,00,00,00,00,00,00,198,00,00,124,198,198,198,198,198,124,00,00,00,00,00,96,48,24,00,124,198,198,198,198,198,124,00,00,00,00,00,48,120,204,00,204,204
db 204,204,204,204,118,00,00,00,00,00,96,48,24,00,204,204,204,204,204,204,118,00,00,00,00,00,00,198,00,00,198,198,198,198,198,198,126,06,12,120,00,00,198,00,124,198,198,198,198,198,198,198,124,00,00,00,00,00,198,00,198,198,198
db 198,198,198,198,198,124,00,00,00,00,00,24,24,126,195,192,192,192,195,126,24,24,00,00,00,00,00,56,108,100,96,240,96,96,96,96,230,252,00,00,00,00,00,00,195,102,60,24,255,24,255,24,24,24,00,00,00,00,00,252,102,102,124,98,102
db 111,102,102,102,243,00,00,00,00,00,14,27,24,24,24,126,24,24,24,24,24,216,112,00,00,00,24,48,96,00,120,12,124,204,204,204,118,00,00,00,00,00,12,24,48,00,56,24,24,24,24,24,60,00,00,00,00,00,24,48,96,00,124,198,198,198,198,198,124
db 00,00,00,00,00,24,48,96,00,204,204,204,204,204,204,118,00,00,00,00,00,00,118,220,00,220,102,102,102,102,102,102,00,00,00,00,118,220,00,198,230,246,254,222,206,198,198,198,00,00,00,00,00,60,108,108,62,00,126,00,00,00,00,00,00
db 00,00,00,00,56,108,108,56,00,124,00,00,00,00,00,00,00,00,00,00,00,48,48,00,48,48,96,192,198,198,124,00,00,00,00,00,00,00,00,00,00,254,192,192,192,192,00,00,00,00,00,00,00,00,00,00,00,254,06,06,06,06,00,00,00,00,00,00,192,192
db 194,198,204,24,48,96,206,155,06,12,31,00,00,00,192,192,194,198,204,24,48,102,206,150,62,06,06,00,00,00,00,24,24,00,24,24,24,60,60,60,24,00,00,00,00,00,00,00,00,00,54,108,216,108,54,00,00,00,00,00,00,00,00,00,00,00,216,108
db 54,108,216,00,00,00,00,00,00,17,68,17,68,17,68,17,68,17,68,17,68,17,68,17,68,85,170,85,170,85,170,85,170,85,170,85,170,85,170,85,170,221,119,221,119,221,119,221,119,221,119,221,119,221,119,221,119,24,24,24,24,24,24,24,24,24
db 24,24,24,24,24,24,24,24,24,24,24,24,24,24,248,24,24,24,24,24,24,24,24,24,24,24,24,24,248,24,248,24,24,24,24,24,24,24,24,54,54,54,54,54,54,54,246,54,54,54,54,54,54,54,54,00,00,00,00,00,00,00,254,54,54,54,54,54,54,54,54,00
db 00,00,00,00,248,24,248,24,24,24,24,24,24,24,24,54,54,54,54,54,246,06,246,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,00,00,00,00,00,254,06,246,54,54,54,54,54,54,54,54,54,54,54,54,54,246,06
db 254,00,00,00,00,00,00,00,00,54,54,54,54,54,54,54,254,00,00,00,00,00,00,00,00,24,24,24,24,24,248,24,248,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,248,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,31,00,00,00,00,00,00
db 00,00,24,24,24,24,24,24,24,255,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,255,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,31,24,24,24,24,24,24,24,24,00,00,00,00,00,00,00,255,00,00,00,00,00,00,00,00,24,24,24,24,24
db 24,24,255,24,24,24,24,24,24,24,24,24,24,24,24,24,31,24,31,24,24,24,24,24,24,24,24,54,54,54,54,54,54,54,55,54,54,54,54,54,54,54,54,54,54,54,54,54,55,48,63,00,00,00,00,00,00,00,00,00,00,00,00,00,63,48,55,54,54,54,54
db 54,54,54,54,54,54,54,54,54,247,00,255,00,00,00,00,00,00,00,00,00,00,00,00,00,255,00,247,54,54,54,54,54,54,54,54,54,54,54,54,54,55,48,55,54,54,54,54,54,54,54,54,00,00,00,00,00,255,00,255,00,00,00,00,00,00,00,00,54,54
db 54,54,54,247,00,247,54,54,54,54,54,54,54,54,24,24,24,24,24,255,00,255,00,00,00,00,00,00,00,00,54,54,54,54,54,54,54,255,00,00,00,00,00,00,00,00,00,00,00,00,00,255,00,255,24,24,24,24,24,24,24,24,00,00,00,00,00,00,00,255
db 54,54,54,54,54,54,54,54,54,54,54,54,54,54,54,63,00,00,00,00,00,00,00,00,24,24,24,24,24,31,24,31,00,00,00,00,00,00,00,00,00,00,00,00,00,31,24,31,24,24,24,24,24,24,24,24,00,00,00,00,00,00,00,63,54,54,54,54,54,54,54,54
db 54,54,54,54,54,54,54,255,54,54,54,54,54,54,54,54,24,24,24,24,24,255,24,255,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,248,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,31,24,24,24,24,24,24,24,24,255,255,255,255,255
db 255,255,255,255,255,255,255,255,255,255,255,00,00,00,00,00,00,00,255,255,255,255,255,255,255,255,255,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,240,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,255
db 255,255,255,255,255,255,00,00,00,00,00,00,00,00,00,00,00,00,00,00,118,220,216,216,216,220,118,00,00,00,00,00,00,120,204,204,204,216,204,198,198,198,204,00,00,00,00,00,00,254,198,198,192,192,192,192,192,192,192,00,00,00
db 00,00,00,00,00,254,108,108,108,108,108,108,108,00,00,00,00,00,00,00,254,198,96,48,24,48,96,198,254,00,00,00,00,00,00,00,00,00,126,216,216,216,216,216,112,00,00,00,00,00,00,00,00,102,102,102,102,102,124,96,96,192,00,00,00
db 00,00,00,00,118,220,24,24,24,24,24,24,00,00,00,00,00,00,00,126,24,60,102,102,102,60,24,126,00,00,00,00,00,00,00,56,108,198,198,254,198,198,108,56,00,00,00,00,00,00,56,108,198,198,198,108,108,108,108,238,00,00,00,00,00,00
db 30,48,24,12,62,102,102,102,102,60,00,00,00,00,00,00,00,00,00,126,219,219,219,126,00,00,00,00,00,00,00,00,00,03,06,126,219,219,243,126,96,192,00,00,00,00,00,00,28,48,96,96,124,96,96,96,48,28,00,00,00,00,00,00,00,124,198,198
db 198,198,198,198,198,198,00,00,00,00,00,00,00,00,254,00,00,254,00,00,254,00,00,00,00,00,00,00,00,00,24,24,126,24,24,00,00,255,00,00,00,00,00,00,00,48,24,12,06,12,24,48,00,126,00,00,00,00,00,00,00,12,24,48,96,48,24,12,00,126
db 00,00,00,00,00,00,14,27,27,27,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,24,216,216,216,112,00,00,00,00,00,00,00,00,24,24,00,126,00,24,24,00,00,00,00,00,00,00,00,00,00,118,220,00,118,220,00,00,00,00,00,00,00,56,108
db 108,56,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,24,24,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,24,00,00,00,00,00,00,00,00,15,12,12,12,12,12,236,108,108,60,28,00,00,00,00,00,216,108,108,108,108,108,00,00,00
db 00,00,00,00,00,00,00,112,216,48,96,200,248,00,00,00,00,00,00,00,00,00,00,00,00,00,124,124,124,124,124,124,124,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,226,128,136,255,226,152,186,255,226,152,187,255,226
db 153,165,255,226,153,166,255,226,153,163,255,226,153,160,255,226,128,162,255,226,151,152,255,226,151,139,255,226,151,153,255,226,153,130,255,226,153,128,255,226,153,170,255,226,153,171,226,153,172,255,226,152,188,255,226,150
db 182,226,150,186,255,226,151,128,226,151,132,255,226,134,149,255,226,128,188,255,194,182,255,194,167,255,226,150,172,255,226,134,168,255,226,134,145,255,226,134,147,255,226,134,146,255,226,134,144,255,226,136,159,226,140,153
db 255,226,134,148,255,226,150,178,255,226,150,188,255,32,194,160,226,128,128,226,128,129,226,128,130,226,128,131,226,128,132,226,128,133,226,128,134,226,128,135,226,128,136,226,128,137,226,128,138,226,128,175,255,33,255,34,255
db 35,255,36,255,37,255,38,255,39,255,40,255,41,255,42,255,43,255,44,255,45,255,46,255,47,255,48,255,49,255,50,255,51,255,52,255,53,255,54,255,55,255,56,255,57,255,58,255,59,255,60,255,61,255,62,255,63,255,64,255,65,255,66,255
db 67,255,68,255,69,255,70,255,71,255,72,255,73,255,74,255,75,255,76,255,77,255,78,255,79,255,80,255,81,255,82,255,83,255,84,255,85,255,86,255,87,255,88,255,89,255,90,255,91,255,92,255,93,255,94,255,95,255,96,255,97,255,98,255
db 99,255,100,255,101,255,102,255,103,255,104,255,105,255,106,255,107,255,108,255,109,255,110,255,111,255,112,255,113,255,114,255,115,255,116,255,117,255,118,255,119,255,120,255,121,255,122,255,123,255,124,255,125,255,126,255,226
db 140,130,255,195,135,255,195,188,255,195,169,255,195,162,255,195,164,255,195,160,255,195,165,255,195,167,255,195,170,255,195,171,255,195,168,255,195,175,255,195,174,255,195,172,255,195,132,255,195,133,226,132,171,255,195,137,255
db 195,166,255,195,134,255,195,180,255,195,182,255,195,178,255,195,187,255,195,185,255,195,191,255,195,150,255,195,156,255,194,162,255,194,163,255,194,165,255,226,130,167,255,198,146,255,195,161,255,195,173,255,195,179,255,195,186
db 255,195,177,255,195,145,255,194,170,255,194,186,255,194,191,255,226,140,144,255,194,172,255,194,189,255,194,188,255,194,161,255,194,171,255,194,187,255,226,150,145,255,226,150,146,255,226,150,147,255,226,148,130,255,226,148,164
db 255,226,149,161,255,226,149,162,255,226,149,150,255,226,149,149,255,226,149,163,255,226,149,145,255,226,149,151,255,226,149,157,255,226,149,156,255,226,149,155,255,226,148,144,255,226,148,148,255,226,148,180,255,226,148,172,255
db 226,148,156,255,226,148,128,255,226,148,188,255,226,149,158,255,226,149,159,255,226,149,154,255,226,149,148,255,226,149,169,255,226,149,166,255,226,149,160,255,226,149,144,255,226,149,172,255,226,149,167,255,226,149,168,255,226
db 149,164,255,226,149,165,255,226,149,153,255,226,149,152,255,226,149,146,255,226,149,147,255,226,149,171,255,226,149,170,255,226,148,152,255,226,148,140,255,226,150,136,255,226,150,132,255,226,150,140,255,226,150,144,255,226,150
db 128,255,206,177,255,195,159,206,178,255,206,147,255,207,128,255,206,163,255,207,131,255,194,181,206,188,255,207,132,255,206,166,255,206,152,255,206,169,226,132,166,255,206,180,255,226,136,158,255,207,134,226,136,133,226,140,128
db 255,206,181,226,136,136,255,226,136,169,255,226,137,161,255,194,177,255,226,137,165,255,226,137,164,255,226,140,160,255,226,140,161,255,195,183,255,226,137,136,255,194,176,255,226,136,153,226,139,133,255,194,183,255,226,136,154
db 255,226,129,191,255,194,178,255,226,136,142,226,150,160,255,194,160,255

;times 10000-($-$$) db 0
;dw 0xAAAA

;times 12500-($-$$) db 0
;dw 0xBBBB

;times 13000-($-$$) db 0
zero:
dq 7

;console_buffer db 0 dup(0)

console_service:



iret



Beep:
    ; open speaker
    in al, 61h
    or al, 00000011b
    out 61h, al
    ; send control word to change frequency
    mov al, 0B6h
    out 43h, al
    ; play frequency 131Hz
    mov ax, 1500
    out 42h, al ; Sending lower byte
    mov al, ah
    out 42h, al ; Sending upper byte 
    ;call delay_cpu

    ; close the speaker
    in al, 61h
    and al, 11111100b
    out 61h, al 
    
ret

times 20000-($-$$) db 0

text db "Hello my friend,\n"
db "Welcome to NahmanOS, the best developed OS in x86 Assembly language\n"
db "I encourage you to explore and test the system, which requires very minimal resources from your computer.\n"
db "Everything is ready, so get going!",0

times 60000-($-$$) db 0

scancodes_table db 0

times 80000-($-$$) db 0
dq 0xBABECAFE
