;Copyright 2019 Liav Albani

;Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

;The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

;THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

org 0xA000
; This Bootloader is intended to test various things before loading kernel:
; 1. Simple graphics mode enabled? Do it (80x25)
; 2. Test architecture. System is allowed to boot only on i586 and above!
; 3. A20 line enabled?
; 4. Preparing IMS (Info Memory Segment) for kernel usage (multiboot specs)
; 5. Mapping Memory allocated and store in IMS
; 6. Mapping linear video buffer, store in IMS
; 7. Sending all the information to an allocated memory segment called IMS 
; 8. Load the kernel executable after every test with paramters about IMS address...
; 9. Pass control to kernel

	

MEM_MAP_ADDR equ 0x7500

stage1:

	;; We are loaded at 0x0:A000
	mov ax,0x0
	mov ds,ax
;; Let's initialize the screen with the BIOS!
	mov ax,03
	int 10h
	mov ax,0B800h
	mov fs,ax

	mov bx,0

	push ax
	push cx
	mov ah,01
	mov ch,0x3f
	int 0x10
	pop cx	
	pop ax
	
;; Stage 2 - Testing Architecture (Using CPUID availability checking)
;; In the odd chance, someone tries to load the system on i486 and below, the bootloader will
;; stop it right here! I assume here that nobody will try to run it on some crude i286 system or below... 
;; (Unless you are a junker with a freaking old system, in which in this case - go away to your old DOS... sorry!)

	jmp testing_arch ; skip some local procedures (e.g. printing a string and clearing the screen)

	error_arch_msg db "ERROR: Undefined CPU",0
	success_arch_msg db "Booting NahmanOS...",0
	error_a20line_msg db "Can't enable A20 line",0
	success_a20line_msg db "A20 line is enabled",0
	loading_init_msg db "Loading init.bin...",0
	success_loading_init_msg db "Loaded init.bin... Executing!",0
	loading_init_msg1 db "Loading Superblock Volume...",0
	loading_init_msg2 db "Loading other information...",0
	loading_init_msg3 db "Loading Block Group Descriptor...",0
	loading_init_msg4 db "Loading init.bin Inode Info....",0

	found_init_bin db "Found init.bin... Loading",0
	not_found_init_bin db "ERROR: init.bin not found",0

ims_structure_pointer db 0xFF
ims_p_mem_map_pointer db 0,0,0,0
ims_p_bootdevice db 0,0,0,0

;; 
; Function: print_message
; Parameters: nothing
; Purpose: Delay a bit the execution
; Returns: nothing
	
delay_cpu:
push ecx
mov ecx,30000
	loop_wait1:
		push ecx
		mov ecx,3000
			loop_wait2:
			nop
			loop loop_wait2
		nop
		pop ecx

	loop loop_wait1
pop ecx
ret

;; 
; Function: print_message
; Parameters: Offset of message, terminated by 0 (C-style string) | Offset in screen
; Purpose: Print message on screen
; Returns: nothing
	
print_message:
	push bp
	mov bp,sp
	push fs
	push bx
	push ax
	push cx
	push si

	mov ax,0xB800
	mov fs,ax
	
	mov bx, [bp+6]
	;mov si, [bp+4]
	mov ah, 70h
	
	
	
	mov si,4000-160
	mov cx,80
	loop_cls_bar:
		
		mov word [fs:si],0x7100
		add si,2		
		loop loop_cls_bar

	mov si,4000-158

	loop_print_msg:
		mov al,[bx]
		cmp al,0
		jz end_printing
		mov [fs:si],ax	
		inc bx
		add si,2
		jmp loop_print_msg


	end_printing:

	pop si
	pop cx
	pop ax
	pop bx
	pop fs
	pop bp
	ret 2*2
;; 
; Function: init_screen
; Purpose: Init_screen procedure, called for clearing screen entirely (in the good old way)...
; Returns: nothing
init_screen:
	push bx
	push ax
	push cx
	xor bx,bx
	mov cx,2000
	loop_cls:
		mov word [fs:bx],0x1F00
		add bx,2		
		loop loop_cls

	mov bx,4000-160
		mov cx,80
	loop_bar:
		
		mov word [fs:bx],0x7F00
		add bx,2		
		loop loop_bar
	pop cx
	pop ax
	pop bx	
	ret



; Function: testing_cpuid_available
; Purpose: Test CPUID availability
; Returns: EAX with various values, 0 = CPUID isn't supported natively (e.g. not i586 or above), else = CPUID is supported
testing_cpuid_available:

	pushfd                               ;Save EFLAGS
	pushfd                               ;Store EFLAGS
	xor dword [esp],0x00200000           ;Invert the ID bit in stored EFLAGS
	popfd                                ;Load stored EFLAGS (with ID bit inverted)
	pushfd                               ;Store EFLAGS again (ID bit may or may not be inverted)
	pop eax                              ;eax = modified EFLAGS (ID bit may or may not be inverted)
	xor eax,[esp]                        ;eax = whichever bits were changed
	popfd                                ;Restore original EFLAGS
	and eax,0x00200000                   ;eax = zero if ID bit can't be changed, else non-zero
	ret

testing_arch: ;; the fun part is starting actually here!

	call init_screen ; ya want a clean screen, don't you?
	call testing_cpuid_available ; testing CPUID availability
	cmp eax,0
	jnz stage3_a20 ; if eax is not 0, we passed this test and can proceed further.
	
	lea bx,[error_arch_msg]
	push bx
	xor bx,bx
	push bx
	call print_message ; call this procedure if we can't boot...
	
	jmp $ ; stop execution because of failure

;; Stage 3 - Enabling A20 Line
;; The approach will be very simple - we will try to test if A20 line is already enabled. If not so - we will try to enable it by ourselves by many techniques...
stage3_a20:

	

	lea bx,[success_arch_msg]
	push bx
	xor bx,bx
	push bx
	call print_message

	call delay_cpu

	jmp check_a20line_main ; go to the main checking a20 line

; Function: check_a20
; Purpose: to check the status of the a20 line in a completely self-contained state-preserving way.
;          The function can be modified as necessary by removing push's at the beginning and their
;          respective pop's at the end if complete self-containment is not required.
; Returns: 0 in ax if the a20 line is disabled (memory wraps around)
;          1 in ax if the a20 line is enabled (memory does not wrap around)
check_a20:
	pushf
	cli ; disabling interrupts
	push ds
	push es
	push di
	push si

	xor ax, ax
	mov es, ax
 
	not ax ; ax = 0xFFFF
	mov ds, ax
 
	mov di, 0x0500
	mov si, 0x0510
 
	mov al, byte [es:di]
	push ax
 
	mov al, byte [ds:si]
	push ax
 
	mov byte [es:di], 0x00
	mov byte [ds:si], 0xFF
 
	cmp byte [es:di], 0xFF
 
	pop ax

	mov byte [ds:si], al
 
	pop ax
	mov byte [es:di], al
 
	mov ax, 0
	je check_a20_exit
 
	mov ax, 1
 
check_a20_exit:
	pop si
	pop di
	pop es
	pop ds
	popf
	ret


; Function: enable_a20line
; Purpose: try to enable A20 Line first with BIOS interrupt and if it's not working - with the Keyboard controller. If failed - stop execution.
; Returns: nothing

enable_a20line:


	;; Try to enable A20 line with bios
	mov     ax,2403h                ;--- A20-Gate Support ---
	int     15h
	jb      a20_failed                  ;INT 15h is not supported
	cmp     ah,0
	jnz     a20_failed                  ;INT 15h is not supported
	 
	mov     ax,2402h                ;--- A20-Gate Status ---
	int     15h
	jb      a20_failed              ;couldn't get status
	cmp     ah,0
	jnz     a20_failed              ;couldn't get status
	 
	cmp     al,1
	jz      a20_activated           ;A20 is already activated
	 
	mov     ax,2401h                ;--- A20-Gate Activate ---
	int     15h
	jb      a20_failed             ;couldn't activate the gate
	cmp     ah,0
	jnz     a20_failed              ;couldn't activate the gate

	a20_failed:
	call enable_a20line_keyboard ; try to enable A20 line with keyboard controller
	call check_a20
	cmp ax,1 ; A20 line is activated
	jmp a20_activated
	
	a20_fault:
		lea bx,[error_a20line_msg]
		push bx
		mov bx,160*2
		push bx
		call print_message
		jmp $ ; stop execution because of failure

	a20_activated:	
	ret

; Function: enable_a20line
; Purpose: try to enable A20 Line with Keyboard controller.
; Returns: nothing

enable_a20line_keyboard:
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        sti
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret


check_a20line_main:
	call check_a20
	cmp ax,1 ; 0 = a20 line is disabled, 1 = enabled
	je stage4_ims ; proceed to next stage
	call enable_a20line ; otherwise enable a20line


;; Stage 4 - Preparing IMS (Info Memory Segment) for kernel usage
;; We will try to first check the amount of available memory.
;; Then calculating the optimised location for us to put the IMS and tables in it.
;; Example: If we have 2GB of RAM available, we will subtract about 100 Megabytes beyond that barrier
;; Then we will place the table there. for stability of kernel, we will implement another table like so
;; 7 bits reserved | IMS available? (1 bit) | 64 bits address (persumably, we will use now only 32 bit addresses) 
;; This data structure will be at 0x1000, physical address.
;; Kernel will try to find that table. If kernel didn't find any table (or first byte is 0)
;; Then kernel will assume that no IMS is available and will have to do every scanning by itself.
;; 
stage4_ims:

	lea bx,[success_a20line_msg]
	push bx
	mov bx,160*1
	push bx
	call print_message ; success with A20 line enabled - we want to inform the user about it.

	

;; Stage 6 - Load the init (init.bin) executable.
;; We will use the BIOS routine handler for loading the file. We assume that the entire disk is formatted with EXT2-FS.
;; We will load the file, in which we assume it resides at the root directory
	;; Load init.bin File to continue execution...


	mov ax,0x50
	mov es,ax

	lea bx,[loading_init_msg]
	push bx
	mov bx,160*1
	push bx
	call print_message ; success with A20 line enabled - we want to inform the user about it.
	
	call delay_cpu


	mov ebx,ebp ; return ESI from EBP



	;mov ecx,4

	;xor ebx,ebx

	;CKPTloop:
      	;mov al, BYTE [ds:si]       ; Get Boot indicator bit flag
      	;test al, 0x80           ; Check For Active Bit
      	;jnz CKPTFound          ; We Found an Active Partition
      	;add bx, 0x10            ; Partition Table Entry is 16 Bytes
      	;dec cx                  ; Decrement Counter
      	;loop CKPTloop           ; Loop
	;CKPTFound:
	;mov cx, si    ; Save Offset
      	;add si, 8               ; Increment Base to LBA Address


			;dword [si]	
	
	mov dword [offset_partition_lba],ebx


; Let's load the Superblock (EXT2 filesystem)
	mov dword [daps1_lba],2 ; 512 bytes per block * 2 blocks
        lea si, [daps1]
	call load_data_from_disk

; Check block size of ext2 blocks from Superblock
	mov ecx,dword [es:0x18]
	mov ebx,1024
 	shl bx,cl
	mov dword [size_of_block_ext2],ebx

	lea bx,[loading_init_msg1]
	push bx
	mov bx,160*1
	push bx
	call print_message

	mov ax,word [es:0x38]
	cmp ax,0xef53 ; check if magic number of ext2 is present
	je continue

	jmp 0:$ ; failure - not suitable filesystem (What?!)

;; Defining Disk Address Packet Structure - for Inode table!
daps1 db 10h
db 0 ; always 0
daps1_sectors dw 4 ; number of sectors to load - 4
daps1_offset dw 0 ; offset to transfer
daps1_segment dw 0x50 ; segment to transfer
daps1_lba dq 2 ; lower 32 bits of 48Bit LBA

offset_partition_lba dd 0
size_of_block_ext2 dd 0

; Function: load_data_from_disk
; Parameters: no parameters
; Purpose: Load Data from Disk
; Returns: nothing
load_data_from_disk:
    push ax
    push dx
    push ebx

    mov ebx,[offset_partition_lba]
    add [daps1_lba],ebx

    mov ah,0x42
    mov dl,0x80
    int 13h
    jc $ ; not working...
    
    pop ebx
    pop dx
    pop ax
ret

; Function: load_blocks_inode
; Parameters: Assuming DAPS is ready, GS:BX contains the segment of inode and offset starting with pointers
; Purpose: Load Data from Disk, according to inode block pointers
; Returns: EDI = amount of blocks loaded
load_blocks_inode:


push gs
push bx

    xor edi,edi ; EDI will be a counter of how many blocks we loaded from inode 2 block pointers
    jmp loop_check_block_pointers
	;mov cx,11 ; check all direct block pointers
    load_blocks:

	push ebx

	mov ebx,edx
	mov eax, dword [size_of_block_ext2]
	mul ebx ; EAX * EBX = Block LBA * size_of_block_ext2
	mov ebx, 512
	div ebx ; EAX / EBX = Block group*size_of_block_ext2/512 bytes of LBA
	mov [daps1_lba],eax ;; EAX is ready, was filled with LBA of direct pointer

	pop ebx
	mov eax, dword [size_of_block_ext2]
	push eax
         lea si, [daps1]
         call load_data_from_disk
	pop eax

	cmp word [daps1_offset],64511
	jae change_segment_offset
	add word [daps1_offset],ax
	jmp added_offset

	change_segment_offset:
	add word [daps1_segment],0x1000
	mov word [daps1_offset],0	

	added_offset:
	 
         inc di
         jmp continue_check_block_pointers
    loop_check_block_pointers:
         mov edx, dword [gs:bx] ; get block of a direct block pointer in inode 2
         cmp edx, 0
         jne load_blocks
	 jmp end_loop_check_pointers
         continue_check_block_pointers:
         add bx,4
         loop loop_check_block_pointers

end_loop_check_pointers:

pop bx
pop gs



ret

; Function: load_blocks_indirect
; Parameters: Assuming DAPS is ready, GS:BX contains the segment of inode and offset starting with pointers
; Purpose: Load Data from Disk, according to inode block pointers
; Returns: EDI = amount of blocks loaded
load_blocks_indirect:


	;;;; load singly indirect block pointer
	add ebx,48; singly indirect block

	;mov ebx,[gs:bx]

	;jmp $

	cmp dword [gs:bx],0
	je end_loading_blocks

	mov ebx,dword [gs:bx]
	xor edx,edx
	mov eax, dword [size_of_block_ext2]
	mul ebx ; EAX * EBX = Block LBA * size_of_block_ext2
	mov ebx, 512
	div ebx ; EAX / EBX = Block group*size_of_block_ext2/512 bytes of LBA
	mov [daps1_lba],eax ;; EAX is ready, was filled with LBA of indirect pointer
	
push edi
push ebx

	mov di,[daps1_segment]
	mov bx,[daps1_offset]

	;jmp $

	mov word [daps1_segment], 0x200
	mov word [daps1_offset],0x0

	push eax
         lea si, [daps1]
         call load_data_from_disk
	pop eax

	mov word [daps1_segment],0x1500 ;di
	mov word [daps1_offset],0x3000  ; 0x2c00 ;bx

pop ebx
pop edi

mov bx,0x200
mov gs,bx
xor ebx,ebx
;mov bx,0x2c00
mov ecx,256
mov edx,dword [gs:bx]
;jmp $
call load_blocks_inode

	;jmp $
	


end_loading_blocks:

ret


; Function: load_ext2block_sectors
; Parameters: no parameters
; Purpose: Load DAPS Sectors value with the appropriate value
; Returns: no returns
load_ext2block_sectors:
	push edx
	push eax
    	push cx
		xor dx,dx
    		mov eax,[size_of_block_ext2]
		mov cx,512
		div cx
		mov word [daps1_sectors],ax ; load only 1 logical ext2 block of data
    	pop cx
    	pop eax
	pop edx
ret

;;;;;;;;;;;;;;;;;;;;;;;END OF FUNCTIONS;;;;;;;;;;;;;;;;;;;;;;

continue:
	
	


;;;;;;;;;;;;;;;; Loading inode table from group block data ;;;;;;;;;;;;;;;;;
    ;; let's prepare some addersing registers for later use. EBX will take the inode table address
    ;; EDI will be a free-willy addressing register (will be used for inodes fetchings) 
    
    mov ebx,dword [es:0x408] ; get the inode table LBA
	mov eax, dword [size_of_block_ext2]

	mul ebx ; EAX * BX = Block group * size_of_block_ext2
	mov ebx, 512
	div ebx ; EAX / BX = Block group*size_of_block_ext2/512 bytes of LBA
    mov [daps1_lba],eax ;; EAX is ready, was filled with LBA of first inode table

    mov dx,0x60
    mov gs,dx ; gs will contain the segment where we will load the inode table

    mov word [daps1_segment],gs
    lea si, [daps1]
    call load_data_from_disk

	

	
;;;;;;;;;;;;;;;; Loading inode 2 block pointers ;;;;;;;;;;;;;;;;;

    ;; we need now to reach inode 2 - which is the root directory...
    mov si,word [es:88] ; get size of inode structure from Superblock
    xor bx,bx ; zero bx
    add bx,si ; we want to access inode 2, so we add the size of one inode sturcture
    add bx, 40 ; Then we want to access the first direct block pointer in inode 2

    mov word [daps1_segment],0x70 ; prepare segment to load..
    mov word [daps1_offset],0
    
	call load_ext2block_sectors
    
    lea si, [daps1]
    mov cx,11 ; check all direct block pointers
    call load_blocks_inode

	push bx

	lea bx,[loading_init_msg2]
	push bx
	mov bx,160*1
	push bx
	call print_message

	pop bx


;;;;;;;;;;;;;;;; Search init.bin File ;;;;;;;;;;;;;;;;;

;; search the root directory for "load2.bin" file and its inode number

    mov eax,dword [size_of_block_ext2] ; get size of block as written in superblock
    mul di ; multiply by amount of blocks we loaded
    mov cx, ax ; move result to cx (this will be our counter for searching)

	

    mov bx,0x70
    mov fs,bx
    xor bx,bx
    
	

    lea si,[file_to_load]


loop_check_string:

	push bx
	push si
	loop_check_equal:
		mov dl,[fs:bx]
		cmp byte [si],0
		je exit_success_check_equal
		cmp dl, byte [si]	
		jne continue_loop_check_equal
		inc bx
		inc si
		jmp loop_check_equal
        continue_loop_check_equal:        
	pop si	
	pop bx
	
	inc bx

        loop loop_check_string
	
	
	lea bx,[not_found_init_bin]
	push bx
	mov bx,160*1
	push bx
	call print_message
	mov ebx,0xFFFFFFFF ; failure!
	jmp $ ; didn't found any file...	

exit_success_check_equal:
pop si
pop bx ; BX contains the offset of filename string


	mov dx,0x1000
	mov gs,dx ; gs will contain the segment where we will load the inode table
	xor dx,dx

jmp continue_2

file_to_load db "init.bin",0x0
	
continue_2:


push bx
	lea bx,[found_init_bin]
	push bx
	mov bx,160*1
	push bx
	call print_message

pop bx

	sub bx,8 ; subtract 8 bytes, so we can access the inode number
	mov edi,dword [fs:bx] ; move inode number to EDX


	mov esi,dword [es:0x28] ; move number of inodes per block group to ESI
	sub edi,1
	mov eax,edi
	div esi ; (EAX) block group =divide (EDI=EAX=inode-1) with ESI (INODES_PER_GROUP)

	mov ebx, dword [size_of_block_ext2]
	mul ebx ; EAX * EBX = Block group * size_of_block_ext2
	mov ebx, 512
	div bx ; EAX = EAX / BX = Block group*size_of_block_ext2/512 bytes of LBA
	add eax,4 ; add to base LBA the block group LBA offset,  base LBA is 4 (offset 2048 bytes / 512 bytes of LBA)


	mov word [ds:daps1_segment],0x1000
	mov word [ds:daps1_offset],0
	
	call load_ext2block_sectors
	

	mov dword [daps1_lba],eax ; LBA of block group offset
        lea si, [daps1]
	call load_data_from_disk


	xor edx,edx

;;;; Loading Inode Table With Correct Block ;;;;;

	mov ebx,dword [gs:0x8] ; get the base inode table LBA from the loaded block group descriptor
	
	

	mov eax,edi ; mov (Inode-1) to EAX
        mov esi,dword [es:0x28] ; move number of inodes per block group to ESI
	div esi ; edx will contain index
	
	
	xor eax,eax

	mov edi, edx ; move Index (EDX) to EDI
	xor edx,edx
	mov ax, word [es:88] ; Inode size from superblock




	mul edi ; EAX = (EDI) index * (AX) Inode size
	mov edi, dword [ds:size_of_block_ext2]
	div edi ; EAX (final result) = EAX ((index * Inode size)) / EDI ((size_of_block_ext2))
	add ebx,eax ; Add to base inode table LBA the amount of blocks to "skip" to reach our inode
	
	
;;;;;;;;;;;;;;;; Load load2.bin Inode Structure ;;;;;;;;;;;;;;;;;

	mov ecx,edx ; edx contains how many bytes we need to skip in the block, move it to ECX
	xor edx,edx ; zero EDX

	mov eax, edi ; move (EDI) size_of_block_ext2 to EAX
	
	mul ebx ; EAX * EBX = Block LBA * size_of_block_ext2
	mov ebx, 512
	div ebx ; EAX / EBX = Block group*size_of_block_ext2/512 bytes of LBA

	mov [daps1_lba],eax ;; EAX is ready, was filled with LBA of direct pointer

	mov word [daps1_segment],0x5000
	mov dword [daps1_lba],eax ; LBA of block group offset
        lea si, [daps1]



	call load_data_from_disk


	mov dx,0x5000
	
	mov gs,dx ; gs will contain the segment where we loaded our inode structure
	xor dx,dx
	mov bx,cx ; we want to access the first direct block pointer in the inode. cx contains how many bytes we need to skip in the block, move it to bx
	mov edx,dword [gs:bx]

;;;;;;;;;;;;;;; Load init.bin Data blocks ;;;;;;;;;;;;;;;;;;;;;

	mov word [daps1_segment],0x1500 ; prepare segment to load..
    	mov word [daps1_offset],0x0
    
	call load_ext2block_sectors

    	lea si, [daps1]
    	mov cx,11 ; check all direct block pointers
	

	add bx,0x28 ; get to the first direct pointer...
push ebx
    	xor edi,edi ; EDI will be a counter of how many blocks we loaded from inode 2 block pointers
	mov cx,11 ; check all direct block pointers
	call load_blocks_inode
pop ebx
	mov dx,0x5000
	
	mov gs,dx ; gs will contain the segment where we loaded our inode structure

	call load_blocks_indirect

	;jmp $

; =================== Finished loading initrd.bin


push bx
	lea bx,[success_loading_init_msg]
	push bx
	mov bx,160*1
	push bx
	call print_message

pop bx

jmp detect_mem

	detect_mem_start db "Detecting memory using BIOS INT 0x15, method 0xE820",0
	detect_mem_finish_e820 db "Finished to detect memory with BIOS INT 0x15, 0xE820 method",0
	detect_mem_error_e820 db "Error to detect memory with 0xE820 method",0
;	detect_mem_start_e801 db "Error to detect memory with 0xE801 method",0

;; Stage 6 - Detect Memory Map
;The methods it uses are:

;    Try BIOS Int 0x15, eax = 0xE820
;    If that didn't work, try BIOS Int 0x15, ax = 0xE801 and BIOS Int 0x12
;    If that didn't work, try BIOS Int 0x15, ah = 0x88 and BIOS Int 0x12 


detect_mem:
xor eax,eax
mov ax,MEM_MAP_ADDR/16
mov es,ax
push bx
	lea bx,[detect_mem_start]
	push bx
	mov bx,160*1
	push bx
	call print_message
pop bx

	call delay_cpu


	xor di,di

	xor ebx,ebx
	mov edx,0x534D4150
	mov eax,0xe820
	mov ecx,24

	int 0x15

	jc continue_next_method ; carry flag = failure of method
	
	cmp eax,0x534D4150
	jne continue_next_method ; EAX != magic number => failure of method
	
loop_detect_e820:
	
	mov edx,0x534D4150
	add di,24 ; next 24 bytes, so we use 24 bytes quantities
	mov eax,0xe820
	mov ecx,24
	
	cmp ebx,0
	je end_detect_mem

	int 0x15

	jc end_detect_mem

	jmp loop_detect_e820
	
continue_next_method:

	jmp $

end_detect_mem:

push bx
	lea bx,[detect_mem_finish_e820]
	push bx
	mov bx,160*1
	push bx
	call print_message
pop bx

call delay_cpu

;; get frame buffer address
cli

push es
    push ax
    push di
    push cx
    push ebx

    mov ax,01000h
    mov es,ax

    xor di,di
    mov ax,4F01h
    mov cx,118h
    ;mov cx,11Bh    
    int 10h

    mov di,40 ; we will take the address out of the info
    ;mov ebx, dword [es:di]

    ;mov dword [0xA00:0x000], ebx

    pop ebx
    pop cx
    pop di    
    pop ax    
pop es 

;;; prepare graphics mode
mov bx, 0x118
;mov bx,0x11B
or bx,0x4000

mov ax,0x4f02
int 10h

;; Stage 7 - Prepare protected mode
;; For supplying a modern environment, we have to enter a protected mode processor-state as soon as possible.
;; We have to setup some tables like GDT and others.



; clear Segments before we move to protected mode
	mov ax,0x0
	mov ds,ax
	mov es,ax
	mov fs,ax
	mov gs,ax

	cli
	
	;; Setting up GDTR for Protected mode
lgdt [gdt_pointer] ; load the gdt table
mov eax, cr0 
or eax,0x1 ; set the protected mode bit on special CPU reg cr0
mov cr0, eax



jmp CODE_SEG:boot2 ; long jump to the code segment

gdt_start:
    dq 0x0
gdt_code:
    dw 0xFFFF
    dw 0x0
    db 0x0
    db 10011010b
    db 11001111b
    db 0x0
gdt_data:
    dw 0xFFFF
    dw 0x0
    db 0x0
    db 10010010b
    db 11001111b
    db 0x0
gdt_end:

gdt_pointer:
    dw gdt_end - gdt_start
    dd gdt_start
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

bits 32

boot2:

cli

mov bx,0xdead

	
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    ;mov fs, ax
    ;mov gs, ax
    mov ss, ax

;    mov esi,hello
;    mov ebx,0xb8002+160



;loop_print:
;    lodsb
;    or al,al
;    jz halt
;    mov ah,0x1F
;    mov word [ebx], ax
;    add ebx,2
;    jmp loop_print

memory_display:

;mov ebx,0xb8002+160


;;mov eax,dword [0x7508]

    
    ;call printNumber
    ;call printCharacter
    ;mov byte [0xb8002],0x1f
;    mov byte [0xb8002],0

halt:

	mov ebx,MEM_MAP_ADDR


	jmp CODE_SEG:0x15000


finish_halt:
mov edx,0
    mov eax,dword [0x7508]
    mov edi,MEM_MAP_ADDR
    cli
    hlt
hello db "Executing Bootstraping NahmanOS init process....",0

printCharacter:
    mov byte [0xb8002],0x1f
    mov byte [0xb8002+1],'D'
    ret


;; Stage 9 - Pass control to kernel
;; We will do last things here, and then pass control to our kernel.
;; This may include final checks and so before we are leaving this area...
;; Presumably here we will setup final preparations like actually entering protected mode.

times 9190-($-$$) db 0	; Pad remainder with 0s
dw 0xFF88
